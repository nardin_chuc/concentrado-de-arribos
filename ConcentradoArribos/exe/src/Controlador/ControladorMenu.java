/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.ConexionDATABASE;
import Vista.IngresarArribos;
import Vista.IngresarEmisorFactura;
import Vista.IngresarFacturas;
import Vista.IngresarPescador;
import Vista.Menu;
import Vista.Resumen;
import Vista.VerArribos;
import Vista.VerFacturas;
import Vista.Vigencias;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;

/**
 *
 * @author CCNAR
 */
public class ControladorMenu implements ActionListener, MouseListener{

    
    Menu mn;
   int año;

    public ControladorMenu(Menu mn) {
        this.mn = mn;
        this.mn.btn_nuevoPescador.addActionListener(this);
        this.mn.btn_VerArribos.addActionListener(this);
        this.mn.btn_VerFacturas.addActionListener(this);
        this.mn.btn_hacerArribos.addActionListener(this);
        this.mn.btn_hacrFacturas.addActionListener(this);
        this.mn.btn_nuevoPescador.addMouseListener(this);
        this.mn.btn_VerArribos.addMouseListener(this);
        this.mn.btn_VerFacturas.addMouseListener(this);
        this.mn.btn_hacerArribos.addMouseListener(this);
        this.mn.btn_hacrFacturas.addMouseListener(this);
        this.mn.btn_emisor.addActionListener(this);
        this.mn.btn_emisor.addMouseListener(this);
        this.mn.btn_resumen.addActionListener(this);
        this.mn.btn_resumen.addMouseListener(this);
        this.mn.btn_Vigencias.addMouseListener(this);
        this.mn.btn_Vigencias.addActionListener(this);
        this.mn.BTN_Configuraciones.addActionListener(this);
        this.mn.BTN_Configuraciones.addMouseListener(this);
        vencidos();
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
     if(e.getSource() == mn.btn_nuevoPescador){
         IngresarPescador Pescador = new IngresarPescador();
         Pescador.setVisible(true);
         mn.setVisible(false);
         
     }else if(e.getSource() == mn.btn_hacerArribos){
         IngresarArribos Arribos = new IngresarArribos();
         Arribos.setVisible(true);
         mn.setVisible(false);
     }else if(e.getSource() == mn.btn_VerArribos){
         VerArribos Varribos = new VerArribos();
         Varribos.setVisible(true);
         mn.setVisible(false);
     }else if(e.getSource() == mn.btn_VerFacturas){
         VerFacturas Vfact = new VerFacturas();
         Vfact.setVisible(true);
         mn.setVisible(false);
     }else if(e.getSource() == mn.btn_hacrFacturas){
         IngresarFacturas fac = new IngresarFacturas();
         fac.setVisible(true);
          mn.setVisible(false);
     }else if(e.getSource() == mn.btn_emisor){
         IngresarEmisorFactura emi = new IngresarEmisorFactura();
         emi.setVisible(true);
         mn.setVisible(false);
     }else if(e.getSource() == mn.btn_resumen){
         Resumen rm = new Resumen();
         rm.setVisible(true);
         mn.setVisible(false);
     }else if(e.getSource() == mn.btn_Vigencias){
         Vigencias rm = new Vigencias();
         rm.setVisible(true);
         mn.setVisible(false);
     }else if(e.getSource() == mn.BTN_Configuraciones){
         ConexionDATABASE rm = new ConexionDATABASE();
         rm.setVisible(true);
         mn.setVisible(false);
     }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
   
    }

    @Override
    public void mousePressed(MouseEvent e) {
   
    }

    @Override
    public void mouseReleased(MouseEvent e) {
   
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    if(e.getSource() == mn.btn_nuevoPescador){
        mn.btn_nuevoPescador.setBackground(Color.red);
    }else if(e.getSource() == mn.btn_VerArribos){
        mn.btn_VerArribos.setBackground(Color.red);
    }else if(e.getSource() == mn.btn_VerFacturas){
       mn.btn_VerFacturas.setBackground(Color.red);
    }else if(e.getSource() == mn.btn_hacerArribos){
        mn.btn_hacerArribos.setBackground(Color.red);
    }else if(e.getSource() == mn.btn_hacrFacturas){
        mn.btn_hacrFacturas.setBackground(Color.red);
    }else if(e.getSource() == mn.btn_resumen){
         mn.btn_resumen.setBackground(Color.red);
    } else if(e.getSource() == mn.btn_emisor){
        mn.btn_emisor.setBackground(Color.red);
    }else if(e.getSource() == mn.btn_Vigencias){
        vencidos();
        mn.lb_vencidos.setVisible(true);
        mn.btn_Vigencias.setBackground(Color.red);
    }else if(e.getSource() == mn.BTN_Configuraciones){
        mn.BTN_Configuraciones.setBackground(Color.red);
    }
    }

    @Override
    public void mouseExited(MouseEvent e) {
    if(e.getSource() == mn.btn_nuevoPescador){
        mn.btn_nuevoPescador.setBackground(Color.white);
    }else if(e.getSource() == mn.btn_VerArribos){
        mn.btn_VerArribos.setBackground(Color.white);
    }else if(e.getSource() == mn.btn_VerFacturas){
       mn.btn_VerFacturas.setBackground(Color.white);
    }else if(e.getSource() == mn.btn_hacerArribos){
        mn.btn_hacerArribos.setBackground(Color.white);
    }else if(e.getSource() == mn.btn_hacrFacturas){
        mn.btn_hacrFacturas.setBackground(Color.white);
    }else if(e.getSource() == mn.btn_resumen){
         mn.btn_resumen.setBackground(Color.white);
    } else if(e.getSource() == mn.btn_emisor){
        mn.btn_emisor.setBackground(Color.white);
    } else if(e.getSource() == mn.btn_Vigencias){
        vencidos();
         mn.btn_Vigencias.setText("Vigencias");
         mn.lb_vencidos.setVisible(true);
         mn.btn_Vigencias.setBackground(Color.GREEN);
    } else if(e.getSource() == mn.BTN_Configuraciones){
        mn.BTN_Configuraciones.setBackground(Color.white);
    }
    }
    
    public void Año(){
        LocalDate currentDate = LocalDate.now();
        año = currentDate.getYear();
        
    }
    
   public void vencidos() {
       int Fiel = vencidosFiel(); 
        try {
            Año();
            int vencidosCant=0;
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT COUNT(id) as cant FROM Permisionario WHERE Fecha_fin_PermisoEscama LIKE '%" + año + "' OR "
                    + "Fecha_fin_PermisoCaracol LIKE '%" + año + "' OR Fecha_fin_PermisoPulpo LIKE '%" + año + "'";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                vencidosCant = rs.getInt("cant");
            }
            
            if(vencidosCant == 0){
                mn.btn_Vigencias.setBackground(Color.WHITE);
                
            }else if(vencidosCant>0){
                mn.btn_Vigencias.setBackground(Color.green);
                mn.btn_Vigencias.setText("Por Vencer ("+(vencidosCant+Fiel)+") este Año");
                mn.lb_vencidos.setText(vencidosCant+" Permisos y "+Fiel+" Firmas");
            }
        } catch (Exception e) {
        }
    }
   
   
   
   
   public int vencidosFiel() {
       int fiel = 0;
        try {
            Año();
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT COUNT(id) as cant FROM Permisionario WHERE Fecha_fin_Fiel LIKE '%" + año + "'";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
                fiel = rs.getInt("cant");
            }
            
            if(fiel == 0){
               return fiel;
                
            }else{
                return fiel;
            }
        } catch (Exception e) {
        }
        return fiel;
    }
}
