/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Conexion;

import Vista.ConexionDATABASE;
import Modelos.Tabla;
import Vista.Menu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Conexion implements ActionListener, MouseListener {

    ConexionDATABASE CDB;

    public Conexion(ConexionDATABASE CDT) {
        this.CDB = CDT;
        this.CDB.btn_ingresar.addActionListener(this);
        this.CDB.btn_modificar.addActionListener(this);
        this.CDB.TB_Servidor.addMouseListener(this);
        this.CDB.btn_Activar.addActionListener(this);
        this.CDB.btn_iniciar.addActionListener(this);
        this.CDB.btn_Eliminar.addActionListener(this);
        this.CDB.btn_nuevo.addActionListener(this);
        Botones(false, true);
        ActualizarId();
        cargarTabla();
    }

    static String servidor = "";
    static String user = "";
    static String password = "";

    public void Server(String server, String user, String contra) {
        if (server.isEmpty()) {

        } else {

        }
    }

    public static Connection establecerConnection() {
        try {
            BuscarServidor();

            String url = "jdbc:sqlserver://" + servidor + ";"
                    + "database=ConcetradoArribos;"
                    + "user=" + user + ";"
                    + "password=" + password + ";"
                    + "loginTimeout=10;";
            Connection con = DriverManager.getConnection(url);
            //JOptionPane.showMessageDialog(null, "se conecto a la base de datos ");
            return con;
        } catch (Exception e) {
            Menu mn = new Menu();
            JOptionPane.showMessageDialog(null, "No se conecto a la base de datos Externa con error: " + e.toString());
            System.exit(0);
//            CDB.txt_server.requestFocus(true);
            //mn.setVisible(false);
            return null;
        }

    }

    public static Connection establecerConnectionInicial() {
        try {
            String url = "jdbc:sqlserver://localhost;"
                    + "database=ConcetradoArribos;"
                    + "user=sa;"
                    + "password=chuc1234;"
                    + "loginTimeout=10;";

            Connection con = DriverManager.getConnection(url);

            //JOptionPane.showMessageDialog(null, "se conecto a la base de datos ");
            return con;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se conecto a la base de datos con error: " + e.toString());
            return null;
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == CDB.btn_ingresar) {
            if (CamposVacios()) {
                //No hace nada
            } else {
                Ingresar();
                cargarTabla();
                ActualizarId();
                Limpiar();
            }

        } else if (e.getSource() == CDB.btn_modificar) {
            if (CamposVacios()) {

            } else {
                Modificar();
                cargarTabla();
                Limpiar();
            }
        } else if (e.getSource() == CDB.btn_nuevo) {
            Botones(false, true);
            ActualizarId();
            cargarTabla();
            Limpiar();
        } else if (e.getSource() == CDB.btn_Activar) {
            if (CamposVacios()) {

            } else {
                Desactivar();
                cargarTabla();
                Limpiar();
            }
        } else if (e.getSource() == CDB.btn_iniciar) {
            BuscarServidor();
            if ((servidor.equals("localhost"))) {
                Menu mn = new Menu();
                mn.setVisible(true);
                CDB.dispose();
            } else {

                int op = JOptionPane.showConfirmDialog(null, "¿Esta seguro de Conectarse a este Servidor?\n"
                        + "   Verifique el Servidor este Encendido\n        Si no Generará Errores", "Verificar Servidor", JOptionPane.ERROR_MESSAGE);
                switch (op) {
                    case 0:
                        Menu mn = new Menu();
                        mn.setVisible(true);
                        CDB.dispose();
                        break;
                    case 1:
                        CDB.txt_server.requestFocus(true);
                        break;
                    default:
                        break;
                }
            }
        } else if (e.getSource() == CDB.btn_Eliminar) {
            Eliminar();
            cargarTabla();
            ActualizarId();
            Limpiar();
        }

    }

    public static void BuscarServidor() {
        try {
            String serv = "";
            String usuario = "";
            String contra = "";
            String Estado = "Activo";
            int vencidosCant = 0;
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnectionInicial();
            String sql = "SELECT*FROM Servidores WHERE Estado = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, Estado);
            rs = ps.executeQuery();

            while (rs.next()) {
                serv = rs.getString("servidor");
                usuario = rs.getString("usuario");
                contra = rs.getString("Contraseña");
            }
            if (serv.isEmpty()) {
                servidor = "localhost";
                user = "sa";
                password = "chuc1234";
            } else {
                servidor = serv;
                user = usuario;
                password = contra;
            }

        } catch (Exception e) {
            System.out.println("Error en Buscar el servidor");
        }
    }

    public boolean CamposVacios() {
        if (CDB.txt_server.getText().isEmpty() || CDB.txt_user.getText().isEmpty() || CDB.txt_contra.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Rellene los campos Vacios", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    public void Ingresar() {
        String server = CDB.txt_server.getText();
        String user = CDB.txt_user.getText();
        String contra = CDB.txt_contra.getText();

        try {
            Connection conectar = Conexion.establecerConnectionInicial();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO Servidores  (servidor, usuario, Contraseña) VALUES (?,?,?)");
            ps.setString(1, server);
            ps.setString(2, user);
            ps.setString(3, contra);
            ps.executeUpdate();
            //    cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso el Servidor correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error ingresar Servidor" + e.getMessage());
        }
    }

    public void Modificar() {
        String server = CDB.txt_server.getText();
        String user = CDB.txt_user.getText();
        String contra = CDB.txt_contra.getText();
        int id = Integer.parseInt(CDB.txt_id.getText());
        try {
            Connection conectar = Conexion.establecerConnectionInicial();
            PreparedStatement ps = conectar.prepareStatement("UPDATE Servidores  SET servidor = ?, usuario = ?, Contraseña = ? WHERE ID = ?");
            ps.setString(1, server);
            ps.setString(2, user);
            ps.setString(3, contra);
            ps.setInt(4, id);
            ps.executeUpdate();
            //    cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso el Servidor correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error ingresar Servidor" + e.getMessage());
        }
    }

    public void Desactivar() {
        int id = Integer.parseInt(CDB.txt_id.getText());
        String Estado = "Inactivo";
        if (CDB.btn_Activar.getText().equals("Activar")) {
            try {
                Connection conectar = Conexion.establecerConnectionInicial();
                PreparedStatement ps = conectar.prepareStatement("UPDATE Servidores SET Estado = ? ");
                ps.setString(1, Estado);
                ps.executeUpdate();
                cargarTabla();
                CDB.btn_Activar.setText("Activar");
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
            String Estado2 = "Activo";
            try {
                Connection conectar = Conexion.establecerConnectionInicial();
                PreparedStatement ps = conectar.prepareStatement("UPDATE Servidores SET Estado = ? WHERE ID = ?");
                ps.setString(1, Estado2);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
                JOptionPane.showMessageDialog(null, "Se Activo El Servidor correctamente");
                CDB.btn_Activar.setText("Activar");
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        } else if (CDB.btn_Activar.getText().equals("Desactivar")) {
            String Estado2 = "Inactivo";
            try {
                Connection conectar = Conexion.establecerConnectionInicial();
                PreparedStatement ps = conectar.prepareStatement("UPDATE Servidores SET Estado = ? WHERE ID = ?");
                ps.setString(1, Estado2);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
                JOptionPane.showMessageDialog(null, "Se Desactivo el Servidor correctamente");
                CDB.btn_Activar.setText("Activar");
            } catch (Exception e) {
                //System.out.println("error: "+e.getMessage());
            }
        }

    }

    public void Eliminar() {
        int id = Integer.parseInt(CDB.txt_id.getText());
        try {
            Connection conectar = Conexion.establecerConnectionInicial();
            PreparedStatement ps = conectar.prepareStatement("DELETE Servidores WHERE ID = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Elimino el Servidor correctamente");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void Limpiar() {
        CDB.txt_user.setText("");
        CDB.txt_contra.setText("");
        CDB.txt_server.setText("");
        CDB.txt_Estado.setText("");
    }

    public void cargarTabla() {
        Tabla tableColor = new Tabla();
        CDB.TB_Servidor.setDefaultRenderer(CDB.TB_Servidor.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) CDB.TB_Servidor.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {210, 120, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            CDB.TB_Servidor.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnectionInicial();
            String sql = "SELECT servidor, usuario, Contraseña, Estado FROM Servidores ORDER BY id ASC, Estado ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == CDB.TB_Servidor) {
            try {
                int fila = CDB.TB_Servidor.getSelectedRow();
                String Servidor = CDB.TB_Servidor.getValueAt(fila, 0).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnectionInicial();
                ps = con.prepareStatement("SELECT * FROM Servidores WHERE servidor = ?");
                ps.setString(1, Servidor);
                rs = ps.executeQuery();
                while (rs.next()) {
                    CDB.txt_id.setText("" + rs.getInt("ID"));
                    CDB.txt_server.setText(rs.getString("servidor"));
                    CDB.txt_user.setText("" + rs.getString("usuario"));
                    CDB.txt_contra.setText("" + rs.getString("Contraseña"));
                    CDB.txt_Estado.setText(rs.getString("Estado"));

                }
                Botones(true, false);
                if (CDB.txt_Estado.getText().equals("Inactivo")) {
                    CDB.btn_Activar.setText("Activar");
                } else if (CDB.txt_Estado.getText().equals("Activo")) {
                    CDB.btn_Activar.setText("Desactivar");
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnectionInicial();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM Servidores");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            CDB.txt_id.setText("" + idAux);
        } catch (Exception e) {
            System.err.println("error ActFolio: " + e.getMessage());
        }
    }

    public void Botones(boolean ver, boolean ver2) {
        CDB.btn_Activar.setEnabled(ver);
        CDB.btn_Eliminar.setEnabled(ver);
        CDB.btn_ingresar.setEnabled(ver2);
        CDB.btn_modificar.setEnabled(ver);

    }

}
