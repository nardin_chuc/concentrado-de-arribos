/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.IngresarArribos;
import Vista.frmRegistroArriboPDF;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorArribosIngresar implements ActionListener, KeyListener, MouseListener {

    IngresarArribos arr;

    int id, Num_Embarcacion;
    String Nombre, Apellido, Contraseña, Nombre_Embarcacion, RNPA, PermisoEscama, PermisoPulpo, PermisoCaracol,
            FechaInicioPermiso, FechaFinPermiso, FechaFielInicio, FechaFielFin, Estado;

    //CONTRUCTOR, EN DONDE SE INICIALIZA TODOS LO EVENTTOS
    public ControladorArribosIngresar(IngresarArribos arr) {
        this.arr = arr;
        this.arr.txt_id.addKeyListener(this);
        this.arr.cbx_Especie.addKeyListener(this);
        arr.txt_dias.addKeyListener(this);
        arr.btnModificar.addActionListener(this);
        arr.btn_registroPDF.addActionListener(this);
        arr.btn_AceptarIngreso.addActionListener(this);
        arr.btn_copy.addActionListener(this);
        arr.btn_registrarArribo.addActionListener(this);
        arr.btn_registrarArribo.addMouseListener(this);
        this.arr.tb_Permisionario.addMouseListener(this);
        this.arr.btn_nuevo.addActionListener(this);
        this.arr.btn_Eliminar.addActionListener(this);
        this.arr.txt_Contraseña.addKeyListener(this);
        this.arr.btn_registrarArribo.addKeyListener(this);
        this.arr.txt_Mes.addKeyListener(this);
        this.arr.txt_AÑO.addKeyListener(this);
        this.arr.cbx_Especie.addMouseListener(this);
        Botones(false, false, false);
        LLenarCBX();
    }

// ******************** METODO EN DONDE RECIBE LOE EVENTOS DE BOTONES EL LA VISTA HACER ARRIBOS
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == arr.btn_registrarArribo) {
            if (arr.txt_Precio.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Dijite El precio");
            } else {
                Ingresar();
                Botones(true, true, true);
                Limpiar();
            }

        } else if (e.getSource() == arr.btn_AceptarIngreso) {
            int op = JOptionPane.showConfirmDialog(null, "¿ Terminar Registro ? ");

            if (op == 0) {
                cargarTabla();
                ModificarFolio();
                ActualizarId();
                Limpiar();
                limpiarTable();
                arr.txt_id.setText("");
                arr.txt_id.requestFocus(true);
            } else {
                arr.txt_Folio.requestFocus(true);
            }
        } else if (e.getSource() == arr.btnModificar) {
            if (arr.txt_id.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Dijite el id del Arribo", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            } else {
                if (arr.txt_dias.getText().isEmpty()) {
                    arr.txt_dias.setText("0");
                    Modificar();
                } else {
                    Modificar();
                }

            }
        } else if (e.getSource() == arr.btn_nuevo) {
            Limpiar();
            ActualizarId();
            arr.cbx_Especie.requestFocus(true);
            Botones(false, false, true);
        } else if (e.getSource() == arr.btn_Eliminar) {
            EliminarRegistro();
            Limpiar();
            arr.cbx_Especie.requestFocus(true);

        } else if (e.getSource() == arr.btn_registroPDF) {
            if (arr.txt_NombreArribo.getText().isEmpty() || arr.txt_idArribo.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Dijite el id del Arribo", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
                return;
            }
            frmRegistroArriboPDF frm = new frmRegistroArriboPDF(arr.txt_NombreArribo.getText(), arr.txt_Apellido.getText());
            frm.setLocationRelativeTo(null);
            frm.setVisible(true);
        } else if (e.getSource() == arr.btn_copy) {
            CopyPassword();
        }
    }

//****** LIMPIA TODOS LOS ESPACIOS EN DONDE SE EDITO
    public void Limpiar() {
        arr.txt_Folio.setText("");
        arr.txt_dias.setText("");
        arr.txt_Precio.setText("");
        arr.txt_ProdEscama.setText("");
        arr.txt_ProdPulpo.setText("");
        arr.txt_ProdCaracol.setText("");

    }

    /// ********* MODIFICA LOS BOTONES DE LA VENTANA, PONIENDOLO A DISPOSICION O NO
    public void Botones(boolean ver, boolean ver1, boolean ver2) {
        arr.btnModificar.setEnabled(ver);
        arr.btn_Eliminar.setEnabled(ver);
        arr.btn_AceptarIngreso.setEnabled(ver1);
        arr.btn_registrarArribo.setEnabled(ver2);
    }

    //******* ACTUALIZA ID DESDE LA BASE DE DATOS SQL SERVER
    public void ActualizarId() {
        int idAux = 0;
        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM " + nombreTabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            rs.close();
            arr.txt_idArribo.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    //******** ELIMINA CUALQUIER REGISTRO QUE SE DESEA BORRAR DE LA BASE DE DATOS
    public void EliminarRegistro() {
        id = Integer.parseInt(arr.txt_idArribo.getText());
        String especie = arr.cbx_Especie.getSelectedItem().toString();
        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("DELETE " + nombreTabla + " WHERE id = ? AND Especie = ?");
            ps.setInt(1, id);
            ps.setString(2, especie);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Elimino el registro seleccionado correctamente");
            ps.close();
        } catch (Exception e) {
            //System.out.println("error: "+e.getMessage());
        }
    }

    //*********** INGRESA LOS AVISOS DE ARRIBOS, ESTE ES LLAMADO EN EL EVENTO DEL BOTON INGRESAR
    public void Ingresar() {
        String año = arr.txt_AÑO.getText();
        String mes = arr.txt_Mes.getText();
        String fecha = arr.txt_fecha.getText();
        id = Integer.parseInt(arr.txt_idArribo.getText());
        String folio = arr.txt_Folio.getText();
        String Especie = arr.cbx_Especie.getSelectedItem().toString();
        int produccion_escama = Integer.parseInt(arr.txt_ProdEscama.getText());
        int produccion_Pulpo = Integer.parseInt(arr.txt_ProdPulpo.getText());
        int produccion_Caracol = Integer.parseInt(arr.txt_ProdCaracol.getText());
        int diaPesca = Integer.parseInt(arr.txt_dias.getText());
        float precio = Float.parseFloat(arr.txt_Precio.getText());
        float totalEscama = produccion_escama * precio;
        float totalPulpo = produccion_Pulpo * precio;
        float totalCaracol = produccion_Caracol * precio;
        String estado = "Activo";
        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO " + nombreTabla
                    + "(AÑO, MES, Fecha,id,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "precio, DiasDe_Pesca, TotalEscama, TotalPulpo, TotalCaracol, Estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, año);
            ps.setString(2, mes);
            ps.setString(3, fecha);
            ps.setInt(4, id);
            ps.setString(5, folio);
            ps.setString(6, Especie);
            ps.setInt(7, produccion_escama);
            ps.setInt(8, produccion_Pulpo);
            ps.setInt(9, produccion_Caracol);
            ps.setFloat(10, precio);
            ps.setInt(11, diaPesca);
            ps.setFloat(12, totalEscama);
            ps.setFloat(13, totalPulpo);
            ps.setFloat(14, totalCaracol);
            ps.setString(15, estado);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso el arribo correctamente");
            ps.close();
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    // ****************** MODIFICA LOS ATRIBUTOS DEL AVISO DE ARRIBO EN LA BASE DE DATOS, ESTE ES LLAMADO EN
    //EL EVENTO DEL BOTON MODIFICAR
    public void Modificar() {
        String año = arr.txt_AÑO.getText();
        String mes = arr.txt_Mes.getText();
        String fecha = arr.txt_fecha.getText();
        id = Integer.parseInt(arr.txt_idArribo.getText());
        String folio = arr.txt_Folio.getText();
        String Especie = arr.cbx_Especie.getSelectedItem().toString();
        String auxEspecie = arr.txt_auxEspecie.getText();
        int produccion_escama = Integer.parseInt(arr.txt_ProdEscama.getText());
        int produccion_Pulpo = Integer.parseInt(arr.txt_ProdPulpo.getText());
        int produccion_Caracol = Integer.parseInt(arr.txt_ProdCaracol.getText());
        int diaPesca = Integer.parseInt(arr.txt_dias.getText());
        float precio = Float.parseFloat(arr.txt_Precio.getText());

        float totalEscama = produccion_escama * precio;
        float totalPulpo = produccion_Pulpo * precio;
        float totalCaracol = produccion_Caracol * precio;

        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + nombreTabla
                    + " SET AÑO = ?, MES = ?, Fecha = ?, Folio = ?, Especie = ?, Produccion_Escama = ?, Produccion_Pulpo = ?, Produccion_Caracol = ?, "
                    + "precio = ?, DiasDe_Pesca = ?, TotalEscama = ?, TotalPulpo = ?, TotalCaracol = ? WHERE id = ? AND Especie = ?");
            ps.setString(1, año);
            ps.setString(2, mes);
            ps.setString(3, fecha);
            ps.setString(4, folio);
            ps.setString(5, Especie);
            ps.setInt(6, produccion_escama);
            ps.setInt(7, produccion_Pulpo);
            ps.setInt(8, produccion_Caracol);
            ps.setFloat(9, precio);
            ps.setInt(10, diaPesca);
            ps.setFloat(11, totalEscama);
            ps.setFloat(12, totalPulpo);
            ps.setFloat(13, totalCaracol);
            ps.setInt(14, id);
            ps.setString(15, auxEspecie);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Modifico el arribo correctamente");
            ps.close();
        } catch (Exception e) {
            System.out.println("error modificar " + e.getMessage());
        }
    }

    //MODIFICA EL FOLIO DEL ARRIBO, DEPENDIENDO DEL ID
    public void ModificarFolio() {

        id = Integer.parseInt(arr.txt_idArribo.getText());
        String folio = arr.txt_Folio.getText();
        int dias = Integer.parseInt(arr.txt_dias.getText());
        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + nombreTabla + " SET Folio = ?  WHERE id = ? ");

            ps.setString(1, folio);
            ps.setInt(2, id);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se finalizo la accion correctamente");
            ps.close();
        } catch (Exception e) {
            System.out.println("error modificarFolio " + e.getMessage());
        }
    }

    public void limpiarTable() {
        DefaultTableModel modeloTabla = (DefaultTableModel) arr.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            modeloTabla.removeRow(i);
            i = i - 1;
        }
    }

    //******** CARGA LA TABLA DESDE LA BASE DE DATOS, DE LOS ARRIBOS REGISTRADOS
    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) arr.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        String mes = arr.txt_Mes.getText();
        String año = arr.txt_AÑO.getText();
        String idPer = arr.txt_id.getText();
        ResultSet rs;
        ResultSetMetaData rsmd;
        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        int columnas;
        int[] ancho = {70, 100, 180, 150, 100, 100, 100, 100, 100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            arr.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();

            String consulta = "SELECT id, Fecha, Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "precio, DiasDe_Pesca FROM " + nombreTabla + " ORDER BY id ASC";
            String sql = "SELECT id, Fecha,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "precio, DiasDe_Pesca FROM " + nombreTabla + " WHERE AÑO =" + año + " AND MES LIKE '%" + mes + "%'ORDER BY id ASC";
            if (idPer.isEmpty()) {
                ps = con.prepareStatement("");
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
                rs.close();
            } else if (mes.isEmpty() || año.isEmpty()) {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
                rs.close();
            } else {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
                rs.close();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n"
                    + "Verifique el id", "Error", JOptionPane.ERROR_MESSAGE);
            arr.txt_id.requestFocus(true);
        }
    }

    public void buscarVigenciaPermisos() {
        try {
            int id = Integer.parseInt(arr.txt_id.getText());
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String consulta = "SELECT *FROM Permisionario WHERE id = " + id;
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            String especie = arr.cbx_Especie.getSelectedItem().toString();
            while (rs.next()) {
                if (especie.equals("PULPO")) {
                    arr.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoPulpo"));
                } else if (especie.equals("CARACOL")) {
                    arr.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoCaracol"));
                } else {
                    arr.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoEscama"));
                }
            }
            rs.close();
        } catch (Exception e) {
        }
    }

    //NO HACE ND
    public void keyTyped(KeyEvent e) {

    }

    //*****EVENTO DEL TECLADO AL PONERSE EN EL TXT_ID, PARA PODER REALIZAR CIERTAS ACCIONES
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() == arr.txt_id) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                arr.btn_copy.requestFocus(true);
                ActualizarId();
                cargarTabla();

            }
        } else if (e.getSource() == arr.txt_Contraseña) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                arr.cbx_Especie.requestFocus(true);
            }
        } else if (e.getSource() == arr.cbx_Especie) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                buscarVigenciaPermisos();
                String especie = arr.cbx_Especie.getSelectedItem().toString();
                if (especie.equals("CARACOL")) {
                    arr.txt_dias.setText("0");
                    arr.txt_ProdEscama.setEnabled(false);
                    arr.txt_ProdEscama.setText("0");
                    arr.txt_ProdPulpo.setText("0");
                    arr.txt_ProdCaracol.setEnabled(true);
                    arr.txt_ProdPulpo.setEnabled(false);
                    arr.txt_ProdCaracol.requestFocus(true);
                } else if (especie.equals("PULPO")) {
                    arr.txt_dias.setText("0");
                    arr.txt_ProdEscama.setEnabled(false);
                    arr.txt_ProdCaracol.setText("0");
                    arr.txt_ProdEscama.setText("0");
                    arr.txt_ProdCaracol.setEnabled(false);
                    arr.txt_ProdPulpo.setEnabled(true);
                    arr.txt_ProdPulpo.requestFocus(true);
                } else if (!(especie.equals("PULPO") || especie.equals("CARACOL"))) {
                    arr.txt_ProdCaracol.setText("0");
                    arr.txt_ProdPulpo.setText("0");
                    arr.txt_dias.setText("0");
                    arr.txt_ProdEscama.setEnabled(true);
                    arr.txt_ProdPulpo.setEnabled(false);
                    arr.txt_ProdCaracol.setEnabled(false);
                    arr.txt_ProdEscama.requestFocus(true);
                }
            }
        } else if (e.getSource() == arr.txt_dias) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                if (arr.txt_dias.getText().isEmpty()) {
                    arr.txt_dias.setText("0");
                    arr.btn_registrarArribo.requestFocus(true);
                } else {
                    arr.btn_registrarArribo.requestFocus(true);
                }
            }
        } else if (e.getSource() == arr.btn_registrarArribo) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                if (arr.txt_Precio.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Dijite El precio");
                } else {
                    Ingresar();
                    Limpiar();
                    arr.cbx_Especie.requestFocus(true);
                }
            }
        }
    }

    //********** REALIZA UNA COSULTA EN LA BASE DE DATOS PARA LOCALIZAR LOS DATOS DEL PERMISIONARIO
    @Override
    public void keyReleased(KeyEvent e) {
        int textoid = 0;
        if (e.getSource() == arr.txt_id) {
            if (arr.txt_id.getText().isEmpty()) {
                textoid = 0;
            } else {
                textoid = Integer.parseInt(arr.txt_id.getText());
            }
            if (e.getSource() == arr.txt_id) {
                try {
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection con = Conexion.establecerConnection();
                    String sql = "SELECT * FROM Permisionario";
                    String consulta = "SELECT *FROM Permisionario WHERE id = " + textoid + " AND Estado = 'Activo'";
                    if ("".equalsIgnoreCase(arr.txt_id.getText())) {
                        arr.txt_NombreArribo.setText("");
                        arr.txt_Apellido.setText("");
                        arr.txt_Contraseña.setText("");;
                        arr.lb_num_embarcacion.setText("");;
                        arr.lb_VigPermiso.setText("");;
                        arr.lb_VigFiel.setText("");
                    } else {
                        ps = con.prepareStatement(consulta);
                        rs = ps.executeQuery();
                        String especie = arr.cbx_Especie.getSelectedItem().toString();
                        while (rs.next()) {
                            arr.txt_NombreArribo.setText(rs.getString("Nombre"));
                            arr.txt_Apellido.setText(rs.getString("Apellido"));
                            arr.txt_Contraseña.setText(rs.getString("Contraseña"));
                            arr.lb_num_embarcacion.setText("" + rs.getInt("Num_Embarcacion"));
                            if (especie.equals("PULPO")) {
                                arr.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoPulpo"));
                            } else if (especie.equals("CARACOL")) {
                                arr.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoCaracol"));
                            } else {
                                arr.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoEscama"));
                            }

                            arr.lb_VigFiel.setText(rs.getString("Fecha_fin_Fiel"));
                        }
                        rs.close();
                        Botones(false, false, true);
                    }

                } catch (Exception ev) {
                    JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n "
                            + "o Se Encuentra Inactivo \n"
                            + " Verifique el id", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (e.getSource() == arr.txt_Mes) {
            cargarTabla();
        } else if (e.getSource() == arr.txt_AÑO) {
            cargarTabla();
        }

    }

    //***** EVENTO QUE AFECTA A LA TABLA, PARA PODER COLOCAR LOS DATOS EN SUS CORRESPONDIENTES TXT DE LA VISTA
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == arr.tb_Permisionario) {
            try {
                int fila = arr.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(arr.tb_Permisionario.getValueAt(fila, 0).toString());
                String especie = arr.tb_Permisionario.getValueAt(fila, 3).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
                ps = con.prepareStatement("SELECT AÑO, MES, id, Fecha, Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                        + "precio, DiasDe_Pesca, TotalEscama, TotalPulpo, TotalCaracol FROM " + nombreTabla + " WHERE id = ? AND Especie = ?");
                ps.setInt(1, id);
                ps.setString(2, especie);
                rs = ps.executeQuery();
                while (rs.next()) {
                    arr.txt_AÑO.setText(rs.getString("AÑO"));
                    arr.txt_Mes.setText(rs.getString("MES"));
                    arr.txt_idArribo.setText("" + rs.getInt("id"));
                    arr.txt_fecha.setText(rs.getString("Fecha"));
                    arr.txt_Folio.setText(rs.getString("Folio"));
                    arr.cbx_Especie.setSelectedItem(rs.getString("Especie"));
                    arr.txt_auxEspecie.setText(rs.getString("Especie"));
                    arr.txt_ProdEscama.setText(rs.getString("Produccion_Escama"));
                    arr.txt_ProdPulpo.setText(rs.getString("Produccion_Pulpo"));
                    arr.txt_ProdCaracol.setText(rs.getString("Produccion_Caracol"));
                    arr.txt_Precio.setText("" + rs.getFloat("precio"));
                    arr.txt_dias.setText("" + rs.getInt("DiasDe_Pesca"));
                }
                rs.close();
                arr.txt_Folio.requestFocus();
                Botones(true, true, false);
                arr.txt_ProdCaracol.setEnabled(true);
                arr.txt_ProdPulpo.setEnabled(true);
                arr.txt_ProdEscama.setEnabled(true);
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

        if (e.getSource() == arr.cbx_Especie) {
            LLenarCBX();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getSource() == arr.cbx_Especie) {
            if (arr.cbx_Especie.getSelectedItem().equals("PULPO")) {
                buscarVigenciaPermisos();
            } else if (arr.cbx_Especie.getSelectedItem().equals("CARACOL")) {
                buscarVigenciaPermisos();
            } else if (!(arr.cbx_Especie.getSelectedItem().equals("PULPO") || arr.cbx_Especie.getSelectedItem().equals("CARACOL"))) {
                buscarVigenciaPermisos();
            }

        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public void LLenarCBX() {
        try {
            arr.cbx_Especie.removeAllItems();
            String estado = "Activo";
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Especie FROM Especies WHERE Estado = ?");
            ps.setString(1, estado);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                String Especie = rs.getString("Especie");
                arr.cbx_Especie.addItem(Especie);
                i++;
            }
            rs.close();

        } catch (Exception er) {
            System.err.println("Error en cbx: " + er.toString());
        }

    }

    private void CopyPassword() {
        if (arr.txt_Contraseña.getText().isEmpty()) {
            return;
        }

        StringSelection seleccion = new StringSelection(arr.txt_Contraseña.getText());
        Clipboard portapapeles = Toolkit.getDefaultToolkit().getSystemClipboard();
        portapapeles.setContents(seleccion, null);
        MostrarMsj();
    }

   private void MostrarMsj(){
      var pane = new JOptionPane("Se ha copiado correctamente", JOptionPane.INFORMATION_MESSAGE);
      var dialog = pane.createDialog(null, "Aviso");
      
      new Thread(() -> {
         try{
             Thread.sleep(500);
             dialog.dispose();
         }catch(InterruptedException e){
             e.printStackTrace();
         }
      }).start();
      
      dialog.setVisible(true);
   }
}
