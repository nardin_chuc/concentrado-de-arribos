/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelos;

import Vista.Vigencias;
import java.awt.Color;
import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author CCNAR
 */
public class TablaVigencias extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        try {

            LocalDate currentDate = LocalDate.now();
            int month = currentDate.getYear();
            String fecha_Año = "31/12/" + month;

            Date day = new Date();
            DateFormat formateadorFechaMedia = DateFormat.getDateInstance(DateFormat.DATE_FIELD);
            String fechaHoy = formateadorFechaMedia.format(day);
            if(table.isRowSelected(row)){
            setForeground(Color.white);
            table.setSelectionBackground(new Color(0,120,215));
            }else
            if (table.getValueAt(row, column).toString().equals("Inactivo") || table.getValueAt(row, column).toString().equals(fechaHoy)) {
                setBackground(Color.red);
                setForeground(Color.white);
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }
            String fecha = table.getValueAt(row, 3).toString();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
            Date date1 = dateFormat.parse(fecha);
            Date date2 = dateFormat.parse(fecha_Año);
            if (date1.compareTo(date2) < 0) {
                if (table.getValueAt(row, column).toString().equals(fecha)) {
                    setBackground(Color.green);
                    setForeground(Color.white);
                }
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }

            return this;
        } catch (Exception e) {
            System.out.println("error en fechas " + e.getMessage());
        }
        return this;
    }
}
