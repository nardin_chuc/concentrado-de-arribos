/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.IngresarCliente;
import Vista.IngresarEspecies;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorIngresarEspecies implements ActionListener, MouseListener, KeyListener{
    
    IngresarEspecies pes;

    public ControladorIngresarEspecies(IngresarEspecies pes) {
        this.pes = pes;
        this.pes.btn_Cancelar.addActionListener(this);
        this.pes.btn_Desactivar.addActionListener(this);
        this.pes.btn_Ingresar.addActionListener(this);
        this.pes.btn_Modificar.addActionListener(this);
        this.pes.btn_Nuevo.addActionListener(this);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.btn_Eliminar.addActionListener(this);
        this.pes.txt_Nombre.requestFocus(true);
       cargarTabla();
       ActualizarId();
    }

        @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == pes.btn_Ingresar){ //////******* BTN INGRESAR
            if(CamposVacios() == true){
            }else{
                if(VericarEspecie("La Especie se encuentra en la Base de Datos") == true){
                    Ingresar();
                    Botones(true, true, false, true);
                }else{
                    
                }
            }
        }else if(e.getSource() == pes.btn_Modificar){//////////******* BTN MODIFICAR
            if(CamposVacios() == true){
                
            }else{
                Modificar();
            }
        }else if(e.getSource() == pes.btn_Desactivar){/////////******** BTN ELIMINAR
            if(CamposVacios() == true){
            }else{
                Desactivar();
                Botones(true, true, false,true);
            }
        }else if(e.getSource() == pes.btn_Cancelar){ ///////////////////// ******* BTN CANCELAR
            int res = JOptionPane.showConfirmDialog(null, "¿Desea Cancelar la transaccion?");
            if(res==0){
                Limpiar();
                Botones(true, true, false,true);
            }else{
                
            }
            
        }else if(e.getSource() == pes.btn_Nuevo){
            Limpiar();
            ActualizarId();
            pes.txt_Nombre.requestFocus(true);
            Botones(true, false, true,true);
        }else if(e.getSource() == pes.btn_Eliminar){
            Eliminar();
            Limpiar();
           
            
        }

    }
    
    public boolean CamposVacios() {
        if (pes.txt_Nombre.getText().isEmpty() || pes.txt_id.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Digite los Campos vacios", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }
    
    
    
    
    
     public boolean VericarEspecie(String textosino){
       String cliente = pes.txt_Nombre.getText();
        String RFC = pes.txt_RFC.getText();
        String nombreSql="";
        String especie = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Especie FROM Especies WHERE Especie = ?");
            ps.setString(1, especie);
            rs = ps.executeQuery();
            while (rs.next()) { 
                     especie = (rs.getString("Especie"));
                 }
            if (especie.isEmpty()) {
                return true;
            }else{
                JOptionPane.showMessageDialog(null, textosino);
                return false;
            }
        
        } catch (Exception e) {
           System.out.println(" Error: Verificar "+e.getMessage());
        }
        return false;
    }
     
     
     public void Ingresar() {
        String Especie = pes.txt_Nombre.getText();
        int Precio = Integer.parseInt(pes.txt_RFC.getText());
        
       try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("INSERT INTO Especies  (Especie, Precio) VALUES (?,?)");
        ps.setString(1, Especie);
        ps.setInt(2, Precio);
        ps.executeUpdate();
        cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Ingreso la Especie correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar "+e.getMessage());
        }
    }
     
     
     public void Modificar() {
        int id = Integer.parseInt(pes.txt_id.getText());
        String Especie = pes.txt_Nombre.getText();
        int Precio = Integer.parseInt(pes.txt_RFC.getText()); try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("UPDATE Especies SET Especie = ?, Precio = ? WHERE id = ?");
        ps.setString(1, Especie);
        ps.setInt(2, Precio);
        ps.setInt(3, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Modifico la Especie correctamente");
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }
    }
     
     
     public void Eliminar(){
        int id = Integer.parseInt(pes.txt_id.getText());
        try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("DELETE Especies WHERE id = ?");
        ps.setInt(1, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Elimino La Especie correctamente");
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage());
        }
     }
     
     public void Desactivar() {
        int id = Integer.parseInt(pes.txt_id.getText());
        String Estado = "Inactivo";
       if(pes.btn_Desactivar.getText().equals("Desactivar")){
            try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("UPDATE Especies SET Estado = ? WHERE id = ?");
        ps.setString(1, Estado);
        ps.setInt(2, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Elimino al Cliente correctamente");
        pes.btn_Desactivar.setText("Desactivar");
        } catch (Exception e) {
            System.out.println("error: "+e.getMessage());
        }
        }else if(pes.btn_Desactivar.getText().equals("Reingresar")){
          String  Estado2 = "Activo";
             try {
        Connection conectar = Conexion.establecerConnection();
        PreparedStatement ps = conectar.prepareStatement("UPDATE Especies SET Estado = ? WHERE id = ?");
        ps.setString(1, Estado2);
        ps.setInt(2, id);
        ps.executeUpdate();
        cargarTabla();
        JOptionPane.showMessageDialog(null, "Se Reingreso al Cliente correctamente");
        pes.btn_Desactivar.setText("Desactivar");
        } catch (Exception e) {
          //System.out.println("error: "+e.getMessage());
        }
        }
        
    }
     
     
     public void cargarTabla() {
       DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT * FROM Especies ORDER BY id ASC, Estado ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
     
     
     
     public void ActualizarId(){
           int idAux = 0;
        try {
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM Especies");
                 rs = ps.executeQuery();
                 while (rs.next()) {
                     idAux = (rs.getInt("id_Maximo")+1);                 
                 }
                 pes.txt_id.setText(""+idAux);
           } catch (Exception e) {
               System.err.println("error ActFolio: "+e.getMessage());
           }
       }
     
     
     public void Limpiar() {
        pes.txt_id.setText("");
        pes.txt_Nombre.setText("");
        pes.txt_RFC.setText("");
        pes.txt_estado.setText("");
    }
     
     
     public void Botones(boolean ver, boolean ver1,boolean ver2, boolean ver3){
        pes.btn_Cancelar.setEnabled(ver);
        pes.btn_Desactivar.setEnabled(ver);
        pes.btn_Modificar.setEnabled(ver1);
        pes.btn_Ingresar.setEnabled(ver2);
        pes.btn_Nuevo.setEnabled(ver3);
        
    }
     
     
     
     //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
        try {
                int fila = pes.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(pes.tb_Permisionario.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Especie, Precio, Estado FROM Especies WHERE id = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pes.txt_id.setText(""+rs.getInt("id"));
                    pes.txt_Nombre.setText(rs.getString("Especie"));
                    pes.txt_RFC.setText(""+rs.getInt("Precio"));
                    pes.txt_estado.setText(rs.getString("Estado"));  
                   
                }
                Botones(true, true, false, true);
                if( pes.txt_estado.getText().equals("Inactivo")){
                    pes.btn_Desactivar.setText("Reingresar");
                }else if( pes.txt_estado.getText().equals("Activo")){
                    pes.btn_Desactivar.setText("Desactivar");
                }                 
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
        public void keyTyped(KeyEvent e) {

    }

    //*****EVENTO DEL TECLADO AL PONERSE EN EL TXT_ID, PARA PODER REALIZAR CIERTAS ACCIONES
    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    //********** REALIZA UNA COSULTA EN LA BASE DE DATOS PARA LOCALIZAR LOS DATOS DEL PERMISIONARIO
    @Override
    public void keyReleased(KeyEvent e) {

    }
    
}
