/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.IngresarPescador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorIngresar implements ActionListener, MouseListener, KeyListener {

    IngresarPescador pes;

    int id, Num_Embarcacion;
    String Nombre, Apellido, Contraseña, Nombre_Embarcacion, RNPA, PermisoEscama, PermisoPulpo, PermisoCaracol,
            FechaInicioPermiso, FechaFinPermiso, FechaFielInicio, FechaFielFin, Estado;
    String rutaArribos, rutaResumen;

    //***** CONSTRUCTOR PARA ACCIONAR LOS EVENTOS DE LA VISTA INGRESAR PERMISIONARIO
    public ControladorIngresar(IngresarPescador pes) {
        this.pes = pes;
        this.pes.btn_Cancelar.addActionListener(this);
        this.pes.btn_Eliminar.addActionListener(this);
        this.pes.btn_Eliminar1.addActionListener(this);
        this.pes.btn_Eliminar1.addMouseListener(this);
        this.pes.btn_Ingresar.addActionListener(this);
        this.pes.btn_Modificar.addActionListener(this);
        this.pes.btn_Nuevo.addActionListener(this);
        this.pes.txt_id.addKeyListener(this);
        this.pes.tb_Permisionario.addMouseListener(this);
        Botones(false, false, true, true);
        cargarTabla();
        ActualizarId();
    }

    //*******METODO QUE RECIBE LOS EVENTOS EFECTUADOS
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pes.btn_Ingresar) { //////******* BTN INGRESAR
            if (CamposVacios() == true) {
            } else {
                if (VericarTabla("El permisionario se encuentra en la Base de Datos") == true) {
                    Ingresar();
                    Botones(true, true, false, true);
                } else {
                }
            }

        } else if (e.getSource() == pes.btn_Modificar) {//////////******* BTN MODIFICAR
            if (CamposVacios() == true) {

            } else {
                Nombre = pes.txt_Nombre.getText();
                Apellido = pes.txt_Apellido.getText();
                String NombreTabla = Nombre.replace(" ", "") + "_" + Apellido.replace(" ", "");
                String nombreAux = pes.txt_NombreAUX.getText();
                if (VericarTabla("Se realizo correctamente") == true) {
                    crearCarpeta(NombreTabla);
//                    CrearTablaSQL(Nombre, Apellido);
                    EliminarTabla(NombreTabla);
                    Modificar();
                    Botones(true, true, false, true);
                } else {
                    Modificar();
                    crearCarpeta(NombreTabla);
                }

            }
        } else if (e.getSource() == pes.btn_Eliminar) {/////////******** BTN ELIMINAR
            if (CamposVacios() == true) {
            } else {
                Eliminar();
                Botones(true, true, false, true);
            }
        } else if (e.getSource() == pes.btn_Eliminar1) {/////////******** BTN ELIMINAR del sistema
            if (CamposVacios() == true) {
            } else {
                EliminarRegistro();
                Limpiar();
                Botones(true, true, false, true);
            }
        } else if (e.getSource() == pes.btn_Cancelar) { ///////////////////// ******* BTN CANCELAR
            int res = JOptionPane.showConfirmDialog(null, "¿Desea Cancelar la transaccion?");
            if (res == 0) {
                Limpiar();
                Botones(true, true, false, true);
            } else {

            }

        } else if (e.getSource() == pes.btn_Nuevo) {
            Limpiar();
            ActualizarId();
            cargarTabla();
            Botones(true, false, true, true);
        }

    }

    //********* METODO PARA INGRESAR NUEVOS PERMISIONARIOS A LA BASE DE DATOS
    public void Ingresar() {
        id = Integer.parseInt(pes.txt_id.getText());
        Nombre = pes.txt_Nombre.getText();
        Apellido = pes.txt_Apellido.getText();
        Contraseña = pes.txt_Contraseña.getText();
        Num_Embarcacion = Integer.parseInt(pes.txt_NumLanchas.getText());
        Nombre_Embarcacion = pes.txt_NomLanchas.getText();
        RNPA = pes.txt_RNPA.getText();
        String RNPA_lancha = pes.txt_RNPA_LANCHA.getText();
        String Matricula = pes.txt_Matricula.getText();
        PermisoEscama = pes.txt_PerEscama.getText();
        PermisoPulpo = pes.txt_perPulpo.getText();
        PermisoCaracol = pes.txt_perCaracol.getText();
        String FechaInicioPermisoEsacama = pes.txt_VigenciaPermisoInicioEscama.getText();
        String FechaFinPermisoEsacama = pes.txt_VigenciaFinPermisoEscama.getText();
        String FechaInicioPermisoPulpo = pes.txt_VigenciaPermisoInicioPulpo.getText();
        String FechaFinPermisoPulpo = pes.txt_VigenciaFinPermisoPulpo.getText();
        String FechaInicioPermisoCaracol = pes.txt_VigenciaPermisoInicioCaracol.getText();
        String FechaFinPermisoCaracol = pes.txt_VigenciaFinPermisoCaracol.getText();
        FechaFielInicio = pes.txt_FielInicio.getText();
        FechaFielFin = pes.txt_FielFin.getText();

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO Permisionario "
                    + "(id, Nombre,Apellido, Contraseña, Num_Embarcacion,Nombre_Embarcacion,"
                    + "RNPA, RNPA_Lancha, Matricula, Permiso_Escama, Permiso_Pulpo, Permiso_Caracol, Fecha_Inicio_PermisoEscama,"
                    + "Fecha_fin_PermisoEscama, Fecha_Inicio_PermisoPulpo,Fecha_fin_PermisoPulpo,Fecha_Inicio_PermisoCaracol,"
                    + "Fecha_fin_PermisoCaracol,Fecha_Inicio_Fiel, Fecha_fin_Fiel) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setInt(1, id);
            ps.setString(2, Nombre);
            ps.setString(3, Apellido);
            ps.setString(4, Contraseña);
            ps.setInt(5, Num_Embarcacion);
            ps.setString(6, Nombre_Embarcacion);
            ps.setString(7, RNPA);
            ps.setString(8, RNPA_lancha);
            ps.setString(9,Matricula);
            ps.setString(10, PermisoEscama);
            ps.setString(11, PermisoPulpo);
            ps.setString(12, PermisoCaracol);
            ps.setString(13, FechaInicioPermisoEsacama);
            ps.setString(14, FechaFinPermisoEsacama);
            ps.setString(15, FechaInicioPermisoPulpo);
            ps.setString(16, FechaFinPermisoPulpo);
            ps.setString(17, FechaInicioPermisoCaracol);
            ps.setString(18, FechaFinPermisoCaracol);
            ps.setString(19, FechaFielInicio);
            ps.setString(20, FechaFielFin);
            ps.executeUpdate();
            cargarTabla();
            CrearTablaSQL(Nombre, Apellido);
            JOptionPane.showMessageDialog(null, "Se Ingreso al Permisionario correctamente");
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    //*********** MODIFICA LOS DATOS YA INGRESADOS DEL PERMISIONARIO
    public void Modificar() {
        id = Integer.parseInt(pes.txt_id.getText());
        Nombre = pes.txt_Nombre.getText();
        Apellido = pes.txt_Apellido.getText();
        Contraseña = pes.txt_Contraseña.getText();
        Num_Embarcacion = Integer.parseInt(pes.txt_NumLanchas.getText());
        Nombre_Embarcacion = pes.txt_NomLanchas.getText();
        RNPA = pes.txt_RNPA.getText();
        String RNPA_lancha = pes.txt_RNPA_LANCHA.getText();
        String Matricula = pes.txt_Matricula.getText();
        PermisoEscama = pes.txt_PerEscama.getText();
        PermisoPulpo = pes.txt_perPulpo.getText();
        PermisoCaracol = pes.txt_perCaracol.getText();
        String FechaInicioPermisoEsacama = pes.txt_VigenciaPermisoInicioEscama.getText();
        String FechaFinPermisoEsacama = pes.txt_VigenciaFinPermisoEscama.getText();
        String FechaInicioPermisoPulpo = pes.txt_VigenciaPermisoInicioPulpo.getText();
        String FechaFinPermisoPulpo = pes.txt_VigenciaFinPermisoPulpo.getText();
        String FechaInicioPermisoCaracol = pes.txt_VigenciaPermisoInicioCaracol.getText();
        String FechaFinPermisoCaracol = pes.txt_VigenciaFinPermisoCaracol.getText();
        FechaFielInicio = pes.txt_FielInicio.getText();
        FechaFielFin = pes.txt_FielFin.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE Permisionario SET Nombre = ?,Apellido = ?, Contraseña = ?, Num_Embarcacion = ?,Nombre_Embarcacion = ?,"
                    + "RNPA = ?, RNPA_Lancha = ?, Matricula = ?, Permiso_Escama = ?, Permiso_Pulpo = ?, Permiso_Caracol = ?, Fecha_Inicio_PermisoEscama = ?,"
                    + "Fecha_fin_PermisoEscama = ?, Fecha_Inicio_PermisoPulpo = ?,Fecha_fin_PermisoPulpo = ?,Fecha_Inicio_PermisoCaracol = ?,Fecha_fin_PermisoCaracol = ?,"
                    + " Fecha_Inicio_Fiel = ?, Fecha_fin_Fiel = ? WHERE id = ?");
            ps.setString(1, Nombre);
            ps.setString(2, Apellido);
            ps.setString(3, Contraseña);
            ps.setInt(4, Num_Embarcacion);
            ps.setString(5, Nombre_Embarcacion);
            ps.setString(6, RNPA);
            ps.setString(7, RNPA_lancha);
            ps.setString(8,Matricula);
            ps.setString(9, PermisoEscama);
            ps.setString(10, PermisoPulpo);
            ps.setString(11, PermisoCaracol);
            ps.setString(12, FechaInicioPermisoEsacama);
            ps.setString(13, FechaFinPermisoEsacama);
            ps.setString(14, FechaInicioPermisoPulpo);
            ps.setString(15, FechaFinPermisoPulpo);
            ps.setString(16, FechaInicioPermisoCaracol);
            ps.setString(17, FechaFinPermisoCaracol);
            ps.setString(18, FechaFielInicio);
            ps.setString(19, FechaFielFin);
            ps.setInt(20, id);
            ps.executeUpdate();
            cargarTabla();
            JOptionPane.showMessageDialog(null, "Se Modifico el Permisionario correctamente");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    //COLOCA UNA ETIQUETA EN LA TABLA, QUE YA FUE ELIMINADO, PERO NO SE ELIMINA EN LA BASE DE DATOS
    public void Eliminar() {
        id = Integer.parseInt(pes.txt_id.getText());
        Estado = "Inactivo";
        if (pes.btn_Eliminar.getText().equals("Eliminar")) {
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE Permisionario SET Estado = ? WHERE id = ?");
                ps.setString(1, Estado);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
                JOptionPane.showMessageDialog(null, "Se Elimino al Permisionario correctamente");
                pes.btn_Eliminar.setText("Eliminar");
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        } else if (pes.btn_Eliminar.getText().equals("Reingresar")) {
            String Estado2 = "Activo";
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE Permisionario SET Estado = ? WHERE id = ?");
                ps.setString(1, Estado2);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
                JOptionPane.showMessageDialog(null, "Se Reingreso al Permisionario correctamente");
                pes.btn_Eliminar.setText("Eliminar");
            } catch (Exception e) {
                //System.out.println("error: "+e.getMessage());
            }
        }

    }

    public void EliminarRegistro() {
        id = Integer.parseInt(pes.txt_id.getText());
        int op = JOptionPane.showInternalConfirmDialog(null, "¿Desea Eliminarlo de la base de datos?");
        if (op == 0) {
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("DELETE Permisionario  WHERE id = ?");

                ps.setInt(1, id);
                ps.executeUpdate();
                cargarTabla();
                JOptionPane.showMessageDialog(null, "Se Elimino de la Base de datos al Permisionario correctamente");
               
            } catch (Exception e) {
                //System.out.println("error: "+e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(null, "Operacion Cancelada");
            Limpiar();

        }

    }

    //********** CONDICION EN DONDE VERIFICAR SI LOS TXT NO ESTAN VACIOS, PARA PODER ACCEDER A UNA ACCION
    public boolean CamposVacios() {
        if (pes.txt_Nombre.getText().isEmpty() || pes.txt_Apellido.getText().isEmpty() || pes.txt_Contraseña.getText().isEmpty()
                || pes.txt_NomLanchas.getText().isEmpty() || pes.txt_id.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Digite los Campos vacios", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    //**** VERIFICA EN LA BASE DE DATOS SI NO SE ENCUENTRA REPETIDA EL NOMBRE INGRESADO 
    public boolean VericarTabla(String textosino) {
        String NombreTabla = pes.txt_Nombre.getText().replace(" ", "") + "_" + pes.txt_Apellido.getText().replace(" ", "");
        String nombreSql = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("VerificarTabla ?");
            ps.setString(1, NombreTabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                nombreSql = (rs.getString("NombreTabla"));
            }
            if (nombreSql.isEmpty()) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, textosino);
                System.out.println("nomSQL " + nombreSql);
                return false;
            }

        } catch (Exception e) {
            System.out.println(" Error: Verificar " + e.getMessage());
        }
        return false;
    }

    //Crea Una tabla en SQL SERVER, POR EL MEDIO DE UN PROCEDIMIENTO ALMACENADO
    public void CrearTablaSQL(String Nombre, String Apellido) {
        String NombreTabla = Nombre.replace(" ", "") + "_" + Apellido.replace(" ", "");
        String nombreSql = "";
        crearCarpeta(NombreTabla);
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("EXECUTE CrearTabla ?");
            ps.setString(1, NombreTabla);
            rs = ps.executeQuery();
        } catch (Exception e) {
            // System.out.println(" Error TablaSQL: "+e.getMessage());
        }
    }

    public boolean buscarId() {
        int id = Integer.parseInt(pes.txt_id.getText());
        String nombre = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("SELECT Nombre FROM Permisionario WHERE id = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                nombre = rs.getString("Nombre");
            }
            if (nombre.isEmpty()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            // System.out.println(" Error TablaSQL: "+e.getMessage());
        }
        return false;
    }

    //********* CARGA LA TABLA QUE SE TIENE EN LA BASE DE DATOS
    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 70, 150, 120, 100, 165, 150, 150, 150, 150, 150, 130, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {

            if (pes.txt_id.getText().isEmpty()) {
                id = 0;
                Connection con = Conexion.establecerConnection();
                String sql = "SELECT id, Nombre, Apellido, Contraseña, Num_Embarcacion, Nombre_Embarcacion, "
                        + "RNPA, RNPA_Lancha, Permiso_Escama, Permiso_Pulpo, Permiso_Caracol, Fecha_fin_PermisoEscama,"
                        + "Fecha_fin_PermisoPulpo, Fecha_fin_PermisoCaracol, Fecha_fin_Fiel, Estado"
                        + " FROM Permisionario ORDER BY id ASC, Estado ASC";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else if (buscarId()) {
                Connection con = Conexion.establecerConnection();
                String sql = "SELECT id, Nombre, Apellido, Contraseña, Num_Embarcacion, Nombre_Embarcacion, "
                        + "RNPA, RNPA_Lancha, Permiso_Escama, Permiso_Pulpo, Permiso_Caracol, Fecha_fin_PermisoEscama,"
                        + "Fecha_fin_PermisoPulpo, Fecha_fin_PermisoCaracol, Fecha_fin_Fiel, Estado"
                        + " FROM Permisionario ORDER BY id ASC, Estado ASC";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else {
                int id = 0;
                if (pes.txt_id.getText().isEmpty()) {
                    id = 0;
                } else {
                    id = Integer.parseInt(pes.txt_id.getText());
                }

                Connection con = Conexion.establecerConnection();
                String sql = "SELECT id, Nombre, Apellido, Contraseña, Num_Embarcacion, Nombre_Embarcacion, "
                        + "RNPA, RNPA_Lancha, Permiso_Escama, Permiso_Pulpo, Permiso_Caracol, Fecha_fin_PermisoEscama,"
                        + "Fecha_fin_PermisoPulpo, Fecha_fin_PermisoCaracol, Fecha_fin_Fiel, Estado"
                        + " FROM Permisionario WHERE id = ?";

                ps = con.prepareStatement(sql);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

    //******* ACTUALIZA EL ID DE LOS PERMISIONARIOS, PARA PODER NGRESARLO O MODIFICARLO EN ALGUN FUTURO
    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM Permisionario");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            pes.txt_id.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    //******* LIMPIA LOS CAMPOS EN EL CUAL SE INGRESO O MODIFICO
    public void Limpiar() {
        pes.txt_id.setText("");
        pes.txt_Nombre.setText("");
        pes.txt_Apellido.setText("");
        pes.txt_Contraseña.setText("");
        pes.txt_NumLanchas.setText("");
        pes.txt_NomLanchas.setText("");
        pes.txt_RNPA.setText("");
        pes.txt_Matricula.setText("");
        pes.txt_RNPA_LANCHA.setText("");
        pes.txt_PerEscama.setText("");
        pes.txt_perPulpo.setText("");
        pes.txt_perCaracol.setText("");
        pes.txt_VigenciaPermisoInicioEscama.setText("");
        pes.txt_VigenciaFinPermisoEscama.setText("");
        pes.txt_VigenciaPermisoInicioPulpo.setText("");
        pes.txt_VigenciaFinPermisoPulpo.setText("");
        pes.txt_VigenciaPermisoInicioCaracol.setText("");
        pes.txt_VigenciaFinPermisoCaracol.setText("");
        pes.txt_FielFin.setText("");
        pes.txt_FielInicio.setText("");
        pes.txt_estado.setText("");
        pes.txt_Nombre.requestFocus(true);
    }

    //****** METODO QUE ELIMINA UNA TABLA EN SQL SERVE, EL CUAL SI SE ENCUENTRA REPETIDA, OMITE EL PASO
    //***** EN CASO CONTRARIO, LO CAMBIA
    public void EliminarTabla(String NombreTabla) {
        String NombreTablaAux = pes.txt_NombreAUX.getText();
        try {
            if (NombreTabla.equals(NombreTablaAux)) {

            } else {
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("EXECUTE sp_rename ?, ?");
                ps.setString(1, NombreTablaAux);
                ps.setString(2, NombreTabla);
                rs = ps.executeQuery();
                JOptionPane.showMessageDialog(null, "Se modifico correctamente el nombre del Permisionario","Modificación Exitosa", JOptionPane.INFORMATION_MESSAGE);

            }
        } catch (Exception e) {

        }
    }
    
    
    public void ModificarNombreFile(String nombreCarpeta){
        try {
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        
    }

    //*******  CREA UNA CARPETA EN EL ESCRITORIO CON EL NOMBRE DEL PERMISIONARIO
    public void crearCarpeta(String nombreCarpeta) {
        String año = pes.lb_año.getText();
        String ruta = System.getProperty("user.home");
        rutaArribos = ruta + "\\OneDrive\\Escritorio\\"+año +"_ConcentradoArribos\\" + nombreCarpeta + "\\arribos";
        rutaResumen = ruta + "\\OneDrive\\Escritorio\\ResumenArribos\\" + nombreCarpeta;
        File f = new File(rutaArribos);
        File file = new File(rutaResumen);
        if (f.mkdirs() || file.mkdirs()) {
            System.out.println("Se creo Carpeta");

        } else {
            System.out.println("No se Creo la carpeta");
        }
    }

    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            try {
                int fila = pes.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(pes.tb_Permisionario.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Nombre,Apellido, Contraseña, Num_Embarcacion,Nombre_Embarcacion,"
                        + "RNPA, RNPA_Lancha, Matricula, Permiso_Escama, Permiso_Pulpo, Permiso_Caracol, Fecha_Inicio_PermisoEscama,Fecha_fin_PermisoEscama, "
                        + "Fecha_Inicio_PermisoPulpo,Fecha_fin_PermisoPulpo, Fecha_Inicio_PermisoCaracol,Fecha_fin_PermisoCaracol,"
                        + " Fecha_Inicio_Fiel, Fecha_fin_Fiel, Estado "
                        + "FROM Permisionario WHERE id = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pes.txt_id.setText("" + rs.getInt("id"));
                    pes.txt_Nombre.setText(rs.getString("Nombre"));
                    pes.txt_Apellido.setText(rs.getString("Apellido"));
                    pes.txt_Contraseña.setText(rs.getString("Contraseña"));
                    pes.txt_NumLanchas.setText("" + rs.getInt("Num_Embarcacion"));
                    pes.txt_NomLanchas.setText(rs.getString("Nombre_Embarcacion"));
                    pes.txt_RNPA.setText(rs.getString("RNPA"));
                    pes.txt_RNPA_LANCHA.setText(rs.getString("RNPA_Lancha"));
                    pes.txt_Matricula.setText(rs.getString("Matricula"));
                    pes.txt_PerEscama.setText(rs.getString("Permiso_Escama"));
                    pes.txt_perPulpo.setText(rs.getString("Permiso_Pulpo"));
                    pes.txt_perCaracol.setText(rs.getString("Permiso_Caracol"));
                    pes.txt_VigenciaPermisoInicioEscama.setText(rs.getString("Fecha_Inicio_PermisoEscama"));
                    pes.txt_VigenciaFinPermisoEscama.setText(rs.getString("Fecha_fin_PermisoEscama"));
                    pes.txt_VigenciaPermisoInicioPulpo.setText(rs.getString("Fecha_Inicio_PermisoPulpo"));
                    pes.txt_VigenciaFinPermisoPulpo.setText(rs.getString("Fecha_fin_PermisoPulpo"));
                    pes.txt_VigenciaPermisoInicioCaracol.setText(rs.getString("Fecha_Inicio_PermisoCaracol"));
                    pes.txt_VigenciaFinPermisoCaracol.setText(rs.getString("Fecha_fin_PermisoCaracol"));
                    pes.txt_FielFin.setText(rs.getString("Fecha_fin_Fiel"));
                    pes.txt_FielInicio.setText(rs.getString("Fecha_Inicio_Fiel"));
                    pes.txt_estado.setText(rs.getString("Estado"));

                }
                Botones(true, true, false, true);
                String Nombre2 = pes.txt_Nombre.getText();
                String Apellido2 = pes.txt_Apellido.getText();
                pes.txt_NombreAUX.setText(Nombre2.replace(" ", "") + "_" + Apellido2.replace(" ", ""));
                if (pes.txt_estado.getText().equals("Inactivo")) {
                    pes.btn_Eliminar.setText("Reingresar");
                } else if (pes.txt_estado.getText().equals("Activo")) {
                    pes.btn_Eliminar.setText("Eliminar");
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() == pes.btn_Eliminar1){
            pes.lb_avisoElimina.setVisible(true);
            pes.lb_avisoElimina.setText("Se Eliminara Permanentemente");
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() == pes.btn_Eliminar1){
            pes.lb_avisoElimina.setVisible(false);
            
        }
    }

    public void Botones(boolean ver, boolean ver1, boolean ver2, boolean ver3) {
        pes.btn_Cancelar.setEnabled(ver);
        pes.btn_Eliminar.setEnabled(ver);
        pes.btn_Modificar.setEnabled(ver1);
        pes.btn_Ingresar.setEnabled(ver2);
        pes.btn_Nuevo.setEnabled(ver3);
        pes.btn_Eliminar1.setEnabled(ver);

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == pes.txt_id) {
            cargarTabla();

        }
    }

}
