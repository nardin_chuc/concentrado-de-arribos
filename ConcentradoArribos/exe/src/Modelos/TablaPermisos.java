/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelos;

import java.awt.Color;
import java.awt.Component;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author CCNAR
 */
public class TablaPermisos extends DefaultTableCellRenderer {

    String año = "";
    public TablaPermisos(String año) {
        this.año = año;
    }
    
    

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        try {

            LocalDate currentDate = LocalDate.now();
            int month = currentDate.getYear();
            String fecha_Año = "31/12/" + año;

            Date day = new Date();
            DateFormat formateadorFechaMedia = DateFormat.getDateInstance(DateFormat.DATE_FIELD);
            String fechaHoy = formateadorFechaMedia.format(day);
            if(table.isRowSelected(row)){
            setForeground(Color.white);
            table.setSelectionBackground(new Color(0,120,215));
            }else
            if (table.getValueAt(row, column).toString().equals("Inactivo") || table.getValueAt(row, column).toString().equals(fechaHoy)) {
                setBackground(Color.red);
                setForeground(Color.white);
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }
            String fechaEscama="";
            String fechaPulpo="";
            if(table.getValueAt(row, 4).toString().isEmpty() && !(table.getValueAt(row, 6).toString().isEmpty())){
                fechaEscama = table.getValueAt(row, 6).toString();
                fechaPulpo = table.getValueAt(row, 6).toString();
            }else if (!(table.getValueAt(row, 4).toString().isEmpty()) && (table.getValueAt(row, 6).toString().isEmpty())){
                fechaEscama = table.getValueAt(row, 4).toString();
                fechaPulpo = table.getValueAt(row, 4).toString();
            }
            else{
             fechaEscama = table.getValueAt(row, 4).toString();
             fechaPulpo = table.getValueAt(row, 6).toString();
             
            }
             SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
            Date dateEscama = dateFormat.parse(fechaEscama);
            Date datePulpo = dateFormat.parse(fechaPulpo);
            // Date dateCaracol = dateFormat.parse(fechaCaracol);
            Date dateAño = dateFormat.parse(fecha_Año);
            if (datePulpo.compareTo(dateAño) < 0) {
               if(table.getValueAt(row, column).toString().equals(fechaPulpo)){
                    setBackground(Color.LIGHT_GRAY);
                    setForeground(Color.black);
                }
            } else {
                setBackground(Color.white);
                setForeground(Color.black);
            }
            
            if(dateEscama.compareTo(dateAño) <0){
                if (table.getValueAt(row, column).toString().equals(fechaEscama) ) {
                    setBackground(Color.ORANGE);
                    setForeground(Color.black);
                }
            }
            
            

            return this;
        } catch (Exception e) {
            System.out.println("error en fechas " + e.getMessage());
        }
        return this;
    }
}
