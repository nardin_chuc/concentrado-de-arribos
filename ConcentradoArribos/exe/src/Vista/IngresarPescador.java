/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import Controlador.ControladorIngresar;
import java.time.LocalDate;
import java.time.Month;
import javax.swing.ImageIcon;

/**
 *
 * @author CCNAR
 */
public class IngresarPescador extends javax.swing.JFrame {

    /**
     * Creates new form IngresarPescador
     */
    public IngresarPescador() {
        initComponents();
        ControladorIngresar Cingresar = new ControladorIngresar(this);
       Año();
       setIconImage(new ImageIcon(getClass().getResource("/Img/descargar.jpg")).getImage());
       this.setTitle("Ingresar Pescador");
//        txt_NombreAUX.setVisible(false);
//        txt_estado.setVisible(false);
    }
    public void Año(){
        LocalDate currentDate = LocalDate.now();
        int month = currentDate.getYear();
        lb_año.setText(""+month);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tb_Permisionario = new javax.swing.JTable();
        btn_Eliminar = new javax.swing.JButton();
        btn_Modificar = new javax.swing.JButton();
        btn_Ingresar = new javax.swing.JButton();
        btn_Cancelar = new javax.swing.JButton();
        btn_Nuevo = new javax.swing.JButton();
        txt_estado = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        txt_id = new javax.swing.JTextField();
        txt_Nombre = new javax.swing.JTextField();
        txt_Apellido = new javax.swing.JTextField();
        txt_Contraseña = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txt_NumLanchas = new javax.swing.JTextField();
        txt_NomLanchas = new javax.swing.JTextField();
        txt_RNPA = new javax.swing.JTextField();
        txt_PerEscama = new javax.swing.JTextField();
        txt_perPulpo = new javax.swing.JTextField();
        txt_perCaracol = new javax.swing.JTextField();
        txt_VigenciaPermisoInicioEscama = new javax.swing.JTextField();
        txt_VigenciaFinPermisoEscama = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txt_VigenciaPermisoInicioPulpo = new javax.swing.JTextField();
        txt_VigenciaFinPermisoPulpo = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txt_VigenciaPermisoInicioCaracol = new javax.swing.JTextField();
        txt_VigenciaFinPermisoCaracol = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_RNPA_LANCHA = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txt_Matricula = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txt_FielFin = new javax.swing.JTextField();
        txt_FielInicio = new javax.swing.JTextField();
        txt_NombreAUX = new javax.swing.JTextField();
        lb_año = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        btn_Eliminar1 = new javax.swing.JButton();
        lb_avisoElimina = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(102, 255, 102));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jButton1.setText("Regresar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jButton2.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        jButton2.setText("Cerrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1260, 0, -1, -1));

        jScrollPane1.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N

        tb_Permisionario.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        tb_Permisionario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Nombre", "Apellido", "Contraseña", "#Lanchas", "Nom_Embarcacion", "RNPA_PER", "RNPA_Lancha", "Permiso_Escama", "Permiso_Pulpo", "Permiso_Caracol", "FinPermisEscama", "FinPermisoPulpo", "FinPermisoCaracol", "FechaFinFiel", "Estado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tb_Permisionario);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 1370, 350));

        btn_Eliminar.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        btn_Eliminar.setText("Eliminar");
        btn_Eliminar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(btn_Eliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 210, 120, -1));

        btn_Modificar.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        btn_Modificar.setText("Modificar");
        btn_Modificar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(btn_Modificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 210, 120, -1));

        btn_Ingresar.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        btn_Ingresar.setText("Ingresar");
        btn_Ingresar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(btn_Ingresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 210, 120, -1));

        btn_Cancelar.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        btn_Cancelar.setText("Cancelar");
        btn_Cancelar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(btn_Cancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 210, 120, -1));

        btn_Nuevo.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        btn_Nuevo.setText("Nuevo");
        btn_Nuevo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(btn_Nuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 210, 120, -1));

        txt_estado.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel1.add(txt_estado, new org.netbeans.lib.awtextra.AbsoluteConstraints(1220, 150, 80, -1));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Personales"));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_id.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel2.add(txt_id, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 30, -1));

        txt_Nombre.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel2.add(txt_Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, 100, -1));

        txt_Apellido.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel2.add(txt_Apellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 50, 110, -1));

        txt_Contraseña.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel2.add(txt_Contraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 50, 90, -1));

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel1.setText("Contraseña");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 30, -1, -1));

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel2.setText("id");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 20, -1));

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel3.setText("Nombre");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, -1, -1));

        jLabel4.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel4.setText("Apellido");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 30, -1, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 380, 80));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Permiso"));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_NumLanchas.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_NumLanchas, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 30, -1));

        txt_NomLanchas.setFont(new java.awt.Font("Comic Sans MS", 3, 10)); // NOI18N
        jPanel3.add(txt_NomLanchas, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 50, 170, -1));

        txt_RNPA.setFont(new java.awt.Font("Comic Sans MS", 3, 10)); // NOI18N
        txt_RNPA.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel3.add(txt_RNPA, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 90, -1));

        txt_PerEscama.setFont(new java.awt.Font("Comic Sans MS", 3, 10)); // NOI18N
        txt_PerEscama.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel3.add(txt_PerEscama, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 50, 110, -1));

        txt_perPulpo.setFont(new java.awt.Font("Comic Sans MS", 3, 10)); // NOI18N
        jPanel3.add(txt_perPulpo, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 50, 90, -1));

        txt_perCaracol.setFont(new java.awt.Font("Comic Sans MS", 3, 10)); // NOI18N
        jPanel3.add(txt_perCaracol, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 50, 90, -1));

        txt_VigenciaPermisoInicioEscama.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_VigenciaPermisoInicioEscama, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 100, 80, -1));

        txt_VigenciaFinPermisoEscama.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_VigenciaFinPermisoEscama, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 100, 80, -1));

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel5.setText("VigenciaFin");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 70, 70, -1));

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel6.setText("Lanchas");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        jLabel7.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel7.setText("NomLancha");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel8.setText("RNPA_Lancha");
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 70, -1, -1));

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel9.setText("Escama");
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 20, -1, -1));

        jLabel10.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel10.setText("Pulpo");
        jPanel3.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 20, -1, -1));

        jLabel11.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel11.setText("Caracol");
        jPanel3.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 20, -1, -1));

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel12.setText("VigenciaInicio");
        jPanel3.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 70, -1, -1));

        jLabel16.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel16.setText("VigenciaInicio");
        jPanel3.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 70, -1, -1));

        txt_VigenciaPermisoInicioPulpo.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_VigenciaPermisoInicioPulpo, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 100, 80, -1));

        txt_VigenciaFinPermisoPulpo.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_VigenciaFinPermisoPulpo, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 100, 80, -1));

        jLabel17.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel17.setText("VigenciaFin");
        jPanel3.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 70, 70, -1));

        jLabel18.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel18.setText("VigenciaInicio");
        jPanel3.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 70, -1, -1));

        txt_VigenciaPermisoInicioCaracol.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_VigenciaPermisoInicioCaracol, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 100, 80, -1));

        txt_VigenciaFinPermisoCaracol.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_VigenciaFinPermisoCaracol, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 100, 80, -1));

        jLabel19.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel19.setText("VigenciaFin");
        jPanel3.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 70, 70, -1));

        txt_RNPA_LANCHA.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_RNPA_LANCHA, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, 170, -1));

        jLabel20.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("RNPA_Permisionario");
        jPanel3.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 110, 120, -1));

        jLabel21.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel21.setText("Matricula");
        jPanel3.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, -1, -1));

        txt_Matricula.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel3.add(txt_Matricula, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 220, -1));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 40, 780, 160));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Fiel"));

        jLabel13.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel13.setText("VigenciaInicio");

        jLabel14.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jLabel14.setText("VigenciaFin");

        txt_FielFin.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N

        txt_FielInicio.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 5, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(26, 26, 26))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(txt_FielInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_FielFin, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_FielFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_FielInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 40, 200, 80));

        txt_NombreAUX.setFont(new java.awt.Font("Comic Sans MS", 3, 12)); // NOI18N
        jPanel1.add(txt_NombreAUX, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, 150, -1));

        lb_año.setFont(new java.awt.Font("Comic Sans MS", 3, 24)); // NOI18N
        lb_año.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(lb_año, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 140, 40));

        jLabel15.setFont(new java.awt.Font("Comic Sans MS", 3, 24)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Ingresar Pescadores");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 610, 40));

        btn_Eliminar1.setFont(new java.awt.Font("Comic Sans MS", 3, 14)); // NOI18N
        btn_Eliminar1.setText("BORRAR DEL SISTEMA");
        btn_Eliminar1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel1.add(btn_Eliminar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 210, 200, -1));
        jPanel1.add(lb_avisoElimina, new org.netbeans.lib.awtextra.AbsoluteConstraints(1090, 210, 210, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Menu mn = new Menu();
        mn.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IngresarPescador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IngresarPescador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IngresarPescador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IngresarPescador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IngresarPescador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_Cancelar;
    public javax.swing.JButton btn_Eliminar;
    public javax.swing.JButton btn_Eliminar1;
    public javax.swing.JButton btn_Ingresar;
    public javax.swing.JButton btn_Modificar;
    public javax.swing.JButton btn_Nuevo;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel lb_avisoElimina;
    public javax.swing.JLabel lb_año;
    public javax.swing.JTable tb_Permisionario;
    public javax.swing.JTextField txt_Apellido;
    public javax.swing.JTextField txt_Contraseña;
    public javax.swing.JTextField txt_FielFin;
    public javax.swing.JTextField txt_FielInicio;
    public javax.swing.JTextField txt_Matricula;
    public javax.swing.JTextField txt_NomLanchas;
    public javax.swing.JTextField txt_Nombre;
    public javax.swing.JTextField txt_NombreAUX;
    public javax.swing.JTextField txt_NumLanchas;
    public javax.swing.JTextField txt_PerEscama;
    public javax.swing.JTextField txt_RNPA;
    public javax.swing.JTextField txt_RNPA_LANCHA;
    public javax.swing.JTextField txt_VigenciaFinPermisoCaracol;
    public javax.swing.JTextField txt_VigenciaFinPermisoEscama;
    public javax.swing.JTextField txt_VigenciaFinPermisoPulpo;
    public javax.swing.JTextField txt_VigenciaPermisoInicioCaracol;
    public javax.swing.JTextField txt_VigenciaPermisoInicioEscama;
    public javax.swing.JTextField txt_VigenciaPermisoInicioPulpo;
    public javax.swing.JTextField txt_estado;
    public javax.swing.JTextField txt_id;
    public javax.swing.JTextField txt_perCaracol;
    public javax.swing.JTextField txt_perPulpo;
    // End of variables declaration//GEN-END:variables
}
