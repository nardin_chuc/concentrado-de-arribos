/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.VerArribos;
import Vista.VerFacturas;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorVerFacturas implements KeyListener, MouseListener {

    VerFacturas Ver;

    String Valor, dato;
    int idArribo;

    public ControladorVerFacturas(VerFacturas Ver) {
        this.Ver = Ver;
        this.Ver.txt_id.addKeyListener(this);
        this.Ver.txt_Mes.addKeyListener(this);
        this.Ver.txt_fecha.addKeyListener(this);
        this.Ver.txt_idArribo.addKeyListener(this);
        this.Ver.txt_Folio.addKeyListener(this);
        this.Ver.tb_Permisionario.addMouseListener(this);
        this.Ver.txt_id.requestFocus(true);
        this.Ver.txt_cliente.addKeyListener(this);
        this.Ver.txt_AÑO.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() == Ver.txt_id) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                Ver.txt_Mes.requestFocus(true);
                String año=  Ver.txt_AÑO.getText();
                cargarTabla("", "", año);
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int textoid = 0;
        String año = Ver.txt_AÑO.getText();
        if (e.getSource() == Ver.txt_id) {
              dato = Ver.txt_id.getText();
              VerificarDatos(dato);
        } else if (e.getSource() == Ver.txt_Mes) {
            Valor = Ver.txt_Mes.getText();
            año = Ver.txt_AÑO.getText();
            cargarTabla("", "", año);
        } else if (e.getSource() == Ver.txt_Folio) {
             Valor = Ver.txt_Folio.getText();
            año = Ver.txt_AÑO.getText();
            cargarTabla(Valor, "", año);
        } else if (e.getSource() == Ver.txt_fecha) {
            Valor = Ver.txt_fecha.getText();
            año = Ver.txt_AÑO.getText();
            cargarTabla(Valor, "",año);
        } else if (e.getSource() == Ver.txt_idArribo) {
            String id = Ver.txt_idArribo.getText();
            año = Ver.txt_AÑO.getText();
            cargarTabla("", id,año);
        }else if (e.getSource() == Ver.txt_AÑO) {
            año = Ver.txt_AÑO.getText();
            String id = Ver.txt_idArribo.getText();
            cargarTabla("", id,año);
        } else if (e.getSource() == Ver.txt_cliente) {
            año = Ver.txt_AÑO.getText();
            Valor = Ver.txt_cliente.getText();
            cargarTabla(Valor, "",año);
        } 

    }
    
    public void VerificarDatos(String id){
        int idPermisionario = 0;
        if (id.isEmpty()) {
                idPermisionario = 0;
            } else {
                idPermisionario = Integer.parseInt(id);
            }
        try {
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection con = Conexion.establecerConnection();
                    String sql = "SELECT * FROM ListaFacturas";
                    String consulta = "SELECT * FROM ListaFacturas WHERE id = " + idPermisionario +" AND Estado = 'Activo'";
                    if ("".equalsIgnoreCase(id)){
                        Ver.txt_NombreArribo.setText("");
                        Ver.txt_Apellido.setText("");
                        Ver.lb_VigPermiso.setText("");
                        Ver.lb_VigFiel.setText("");
                        Ver.txt_id.requestFocus();
                    } else {
                        ps = con.prepareStatement(consulta);
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            Ver.txt_NombreArribo.setText(rs.getString("Nombre"));
                            Ver.txt_Apellido.setText(rs.getString("Apellido"));
                            Ver.lb_VigPermiso.setText(rs.getString("Fecha_fin_Permiso"));
                            Ver.lb_VigFiel.setText(rs.getString("Fecha_fin_Fiel"));
                            
                                
                            }
                        
                    }

                } catch (Exception ev) {
                    JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n o esta Inactivo"
                            + "Verifique el id", "Error", JOptionPane.ERROR_MESSAGE);
            }
    }
    
    public void limpiarTable() {
        DefaultTableModel modeloTabla = (DefaultTableModel) Ver.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            modeloTabla.removeRow(i);
            i = i - 1;
        }
    }

    public void cargarTabla(String dato, String id, String año) {
        DefaultTableModel modeloTabla = (DefaultTableModel) Ver.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        String nombreTabla = "Factura_"+Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String mes = Ver.txt_Mes.getText();
        if (id.isEmpty()) {
            idArribo = 0;
        } else {
            idArribo = Integer.parseInt(id);
        }

        int columnas;
        int[] ancho = {90, 50, 110, 90, 90, 90, 90, 300, 90, 80,165, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            Ver.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT Fecha,id,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "Factura, Facturado, Monto, Cliente, Estado  FROM " + nombreTabla + " ORDER BY id ASC";

            String consulta = "SELECT Fecha,id,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "Factura, Facturado, Monto, Cliente, Estado  FROM " + nombreTabla + " WHERE id LIKE '%" + dato + "%' OR Fecha LIKE '%" + dato
                    + "%' OR Factura LIKE '%" + dato + "%' OR Cliente LIKE '%" + dato + "%' ORDER BY id ASC";

            String consultaAño = "SELECT Fecha,id,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "Factura, Facturado, Monto, Cliente, Estado  FROM " + nombreTabla + " WHERE  AÑO = "+año+" AND Mes LIKE '%" + mes +"%' ORDER BY id ASC";
            
            String consultaiD = "SELECT Fecha,id,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "Factura, Facturado, Monto, Cliente, Estado  FROM " + nombreTabla + " WHERE  id = " + idArribo;
            
            if ("".equalsIgnoreCase(año)) {
               limpiarTable();

            } else if (!("".equalsIgnoreCase(año)) && !("".equalsIgnoreCase(mes))) {
                ps = con.prepareStatement(consultaAño);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!("".equalsIgnoreCase(dato)) && !("".equalsIgnoreCase(año))) {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }else if (!("".equalsIgnoreCase(id))) {
                ps = con.prepareStatement(consultaiD);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }
            else if (!("".equalsIgnoreCase(año))) {
                ps = con.prepareStatement(consultaAño);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }

        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
            JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n"
                            + "Verifique el id", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == Ver.tb_Permisionario) {
            try {
                int fila = Ver.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(Ver.tb_Permisionario.getValueAt(fila, 1).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                String nombreTabla = "Factura_"+Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
                ps = con.prepareStatement("SELECT Fecha, Factura FROM " + nombreTabla + " WHERE id = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Ver.Lb_folio.setText(rs.getString("Factura"));
                    Ver.txt_fecha.setText(rs.getString("Fecha"));
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
     }

}
