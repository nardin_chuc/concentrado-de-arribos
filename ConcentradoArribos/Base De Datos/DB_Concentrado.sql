USE [ConcetradoArribos]
GO
/****** Object:  Table [dbo].[Especies]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Especies](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Especie] [varchar](255) NULL,
	[Precio] [int] NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_Especies] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListaFacturas]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListaFacturas](
	[id] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Contraseña] [varchar](50) NULL,
	[RNPA] [varchar](50) NULL,
	[RFC] [varchar](20) NULL,
	[Fecha_Inicio_Permiso] [varchar](50) NULL,
	[Fecha_fin_Permiso] [varchar](50) NULL,
	[Fecha_Inicio_Fiel] [varchar](50) NULL,
	[Fecha_fin_Fiel] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
	[Clientes] [varchar](10) NULL,
 CONSTRAINT [PK_ListaFacturas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permisionario]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permisionario](
	[id] [int] NULL,
	[Nombre] [varchar](255) NULL,
	[Apellido] [varchar](255) NULL,
	[Contraseña] [varchar](255) NULL,
	[Num_Embarcacion] [int] NULL,
	[Nombre_Embarcacion] [varchar](255) NULL,
	[RNPA] [varchar](255) NULL,
	[RNPA_Lancha] [varchar](50) NULL,
	[Matricula] [varchar](50) NULL,
	[Permiso_Escama] [varchar](255) NULL,
	[Permiso_Pulpo] [varchar](255) NULL,
	[Permiso_Caracol] [varchar](255) NULL,
	[Fecha_Inicio_PermisoEscama] [varchar](255) NULL,
	[Fecha_fin_PermisoEscama] [varchar](255) NULL,
	[Fecha_Inicio_PermisoPulpo] [varchar](255) NULL,
	[Fecha_fin_PermisoPulpo] [varchar](255) NULL,
	[Fecha_Inicio_PermisoCaracol] [varchar](255) NULL,
	[Fecha_fin_PermisoCaracol] [varchar](255) NULL,
	[Fecha_Inicio_Fiel] [varchar](255) NULL,
	[Fecha_fin_Fiel] [varchar](255) NULL,
	[Estado] [varchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servidores]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servidores](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[servidor] [varchar](50) NULL,
	[usuario] [varchar](50) NULL,
	[Contraseña] [varchar](50) NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_Servidores] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Especies] ADD  CONSTRAINT [DF_Especies_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[ListaFacturas] ADD  CONSTRAINT [DF_ListaFacturas_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[Permisionario] ADD  CONSTRAINT [DF_Permisionario_Estado]  DEFAULT ('Activo') FOR [Estado]
GO
ALTER TABLE [dbo].[Servidores] ADD  CONSTRAINT [DF_Servidores_Estado]  DEFAULT ('Inactivo') FOR [Estado]
GO
/****** Object:  StoredProcedure [dbo].[CrearClientesTabla]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CrearClientesTabla](
@nombreTabla varchar(75)
)
AS
BEGIN
		Execute ('Create TABLE '+@nombreTabla+'(
		id int,
		Cliente varchar(50),
		RFC_Receptor varchar(20),
		Estado varchar(10))')
END
GO
/****** Object:  StoredProcedure [dbo].[CrearFacturasClientes]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CrearFacturasClientes](
@nombreTabla varchar(75)
)
AS
BEGIN
		Execute ('Create TABLE '+@nombreTabla+'(
		AÑO varchar(10),
		MES varchar(20),
		Fecha varchar(50),
		id int,
		id_factura int,
		Folio varchar(50),
		Especie varchar(50),
		Produccion_Escama int,
		Produccion_Pulpo int,
		Produccion_Caracol int,
		Factura varchar(50),
		Facturado varchar(50),
		Monto int,
		Estado varchar(50),
		Cliente varchar(50),
		RFC_Receptor varchar(20),
		FolioFactura varchar(10),
		FormaPago varchar(50),
		UsoCFI varchar(50),
		RegimenFiscal varchar(70))')
END
GO
/****** Object:  StoredProcedure [dbo].[CrearTabla]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CrearTabla](
@nombreTabla varchar(75)
)
AS
BEGIN
		Execute ('Create TABLE '+@nombreTabla+'(
		AÑO varchar(10),
		MES varchar(20),
		Fecha varchar(50),
		id int,
		Folio varchar(50),
		Especie varchar(50),
		Produccion_Escama int,
		Produccion_Pulpo int,
		Produccion_Caracol int,
		precio float,
		DiasDe_Pesca int,
		TotalEscama float,
		TotalPulpo float,
		TotalCaracol float,
		Estado varchar(50))')
END
GO
/****** Object:  StoredProcedure [dbo].[CrearTablaFacturas]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CrearTablaFacturas](
@nombreTabla varchar(75)
)
AS
BEGIN
		Execute ('Create TABLE '+@nombreTabla+'(
		AÑO varchar(10),
		MES varchar(20),
		Fecha varchar(50),
		id int,
		id_Factura int,
		Folio varchar(50),
		Especie varchar(50),
		Produccion_Escama int,
		Produccion_Pulpo int,
		Produccion_Caracol int,
		Factura varchar(50),
		Facturado varchar(50),
		Monto int,
		Estado varchar(50))')
END
GO
/****** Object:  StoredProcedure [dbo].[VerificarTabla]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VerificarTabla](
@NombreSiExiste varchar(50)
)
AS
BEGIN
SELECT TABLE_NAME AS NombreTabla
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = @NombreSiExiste

END

GO
/****** Object:  StoredProcedure [dbo].[VerificarTablaFacturas]    Script Date: 04/09/2022 10:39:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VerificarTablaFacturas](
@NombreSiExiste varchar(50)
)
AS
BEGIN
SELECT TABLE_NAME AS NombreTabla
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = @NombreSiExiste

END
GO
