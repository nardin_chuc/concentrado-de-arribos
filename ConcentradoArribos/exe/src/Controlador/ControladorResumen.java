/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.Resumen;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CCNAR
 */
public class ControladorResumen implements KeyListener, ActionListener {

    Resumen Ver;

    String Valor, dato;
    int idArribo;

    public ControladorResumen(Resumen Ver) {
        this.Ver = Ver;
        this.Ver.txt_id.addKeyListener(this);
        this.Ver.txt_id.requestFocus(true);
        this.Ver.txt_AÑO.addKeyListener(this);
        this.Ver.btn_imprimir.addActionListener(this);
        Botones(false);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == Ver.txt_id) {
                String info = Ver.txt_id.getText();
                if (verificarDatos(info) == true) {
                    produccionEscama();
                    produccionPulpo();
                    produccionCaracol();
                    ProduccionTotal();
                    IngresoTotal();
                    Botones(true);
                    if (VericarTabla() == true) {
                        Ver.FAC_ENE.setText("");
                        Ver.FAC_FEB.setText("");
                        Ver.FAC_MAR.setText("");
                        Ver.FAC_ABRI.setText("");
                        Ver.FAC_MAY.setText("");
                        Ver.FAC_JUN.setText("");
                        Ver.FAC_JUL.setText("");
                        Ver.FAC_AGO.setText("");
                        Ver.FAC_SEP.setText("");
                        Ver.FAC_OCT.setText("");
                        Ver.FAC_NOV.setText("");
                        Ver.FAC_DIC.setText("");
                    } else {
                        Facturas();
                        TotalFactura();
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo mostrar reporte");
                }
            } else if (e.getSource() == Ver.txt_AÑO) {
                if (Ver.txt_AÑO.getText().equals(Ver.txt_AñoAux.getText())) {
                    Ver.btn_imprimirtodos.setEnabled(true);
                } else {
                    Ver.btn_imprimirtodos.setEnabled(false);
                }
                if (vericarVacioAño()) {
                    limpiar();
                } else {
                    if (Ver.txt_AÑO.getText().equals(Ver.txt_AñoAux.getText())) {
                        Ver.btn_imprimirtodos.setEnabled(true);
                    } else {
                        Ver.btn_imprimirtodos.setEnabled(false);
                    }

                    produccionEscama();
                    produccionPulpo();  ///******
                    produccionCaracol();
                    ProduccionTotal();
                    IngresoTotal();
                    if (VericarTabla() == true) {
                        Ver.FAC_ENE.setText("");
                        Ver.FAC_FEB.setText("");
                        Ver.FAC_MAR.setText("");
                        Ver.FAC_ABRI.setText("");
                        Ver.FAC_MAY.setText("");
                        Ver.FAC_JUN.setText("");
                        Ver.FAC_JUL.setText("");
                        Ver.FAC_AGO.setText("");
                        Ver.FAC_SEP.setText("");
                        Ver.FAC_OCT.setText("");
                        Ver.FAC_NOV.setText("");
                        Ver.FAC_DIC.setText("");
                    } else {
                        Facturas();
                        TotalFactura();
                    }

                }
            }

        }
    }

    public void Botones(boolean ver) {
        Ver.btn_abrirPDF.setEnabled(ver);
        Ver.btn_imprimir.setEnabled(ver);
        Ver.btn_imprimirtodos.setEnabled(ver);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int textoid = 0;
        if (e.getSource() == Ver.txt_id) {
            String dato = Ver.txt_id.getText();
            verificarDatos(dato);
            Ver.btn_imprimirtodos.setEnabled(true);
        }

    }

    public boolean vericarVacioAño() {
        if (Ver.txt_AÑO.getText().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificarDatos(String id) {
        int idPermisionario = 0;
        if (id.isEmpty()) {
            idPermisionario = 0;
            limpiar();
            System.out.println("hola2");
        } else {
            idPermisionario = Integer.parseInt(id);
        }
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT * FROM Permisionario";
            String consulta = "SELECT *FROM Permisionario WHERE id = " + idPermisionario;
            if ("".equalsIgnoreCase(Ver.txt_id.getText())) {
                Ver.txt_NombreArribo.setText("");
                Ver.txt_Apellido.setText("");
                Ver.lb_num_embarcacion.setText("");
                Ver.lb_VigPermiso.setText("");
                Ver.lb_VigFiel.setText("");
                Ver.txt_Contraseña.setText("");
                return false;
            } else {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                while (rs.next()) {
                    Ver.txt_NombreArribo.setText(rs.getString("Nombre"));
                    Ver.txt_Apellido.setText(rs.getString("Apellido"));
                    Ver.txt_Contraseña.setText(rs.getString("Contraseña"));
                    Ver.lb_num_embarcacion.setText("" + rs.getInt("Num_Embarcacion"));
                    Ver.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoEscama"));
                    Ver.lb_VigFiel.setText(rs.getString("Fecha_fin_Fiel"));
                    return true;
                }
            }

        } catch (Exception ev) {
            JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n"
                    + "Verifique el id", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public String buscarProdEscama(String nombreTabla, String mes, String prod) {
        String dato = "";
        int prodMes = 0;
        int dia = 0;
        String año = Ver.txt_AÑO.getText();
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT SUM(Produccion_Escama) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreTabla
                    + " WHERE MES = ? and Especie <> 'PULPO' and Especie <> 'Caracol' AND AÑO = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, mes);
            ps.setString(2, año);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodMes = (rs.getInt("Produccion"));
                dia = (rs.getInt("dias"));
                if (prodMes == 0) {
                    dato = "Sin produccion";
                } else {
                    dato = "" + prodMes + " kg en " + dia + " dia(s)";
                }

            }
            rs.close();

        } catch (Exception e) {

            Ver.txt_id.setText("");
            Ver.txt_id.requestFocus(true);
            System.err.println("error en buscar suma mensual arribos : " + e.getMessage());
        }
        return dato;
    }

    public String buscarProdPulpo(String nombreTabla, String mes, String prod) {
        String dato = "";
        int prodMes = 0;
        int dia = 0;
        String año = Ver.txt_AÑO.getText();
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT SUM(Produccion_Pulpo) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreTabla
                    + " WHERE MES = ? and Especie = 'PULPO'  AND AÑO = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, mes);
            ps.setString(2, año);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodMes = (rs.getInt("Produccion"));
                dia = (rs.getInt("dias"));
                if (prodMes == 0) {
                    dato = "Sin produccion";
                } else {
                    dato = "" + prodMes + " kg en " + dia + " dia(s)";
                }

            }
            rs.close();

        } catch (Exception e) {

            Ver.txt_id.setText("");
            Ver.txt_id.requestFocus(true);
            System.err.println("error en buscar suma mensual arribos : " + e.getMessage());
        }
        return dato;
    }

    public String buscarProdCaracol(String nombreTabla, String mes, String prod) {
        String dato = "";
        int prodMes = 0;
        int dia = 0;
        String año = Ver.txt_AÑO.getText();
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT SUM(Produccion_Caracol) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreTabla
                    + " WHERE MES = ? and Especie = 'Caracol'  AND AÑO = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, mes);
            ps.setString(2, año);
            rs = ps.executeQuery();
            while (rs.next()) {
                prodMes = (rs.getInt("Produccion"));
                dia = (rs.getInt("dias"));
                if (prodMes == 0) {
                    dato = "Sin produccion";
                } else {
                    dato = "" + prodMes + " kg en " + dia + " dia(s)";
                }

            }
            rs.close();

        } catch (Exception e) {

            Ver.txt_id.setText("");
            Ver.txt_id.requestFocus(true);
            System.err.println("error en buscar suma mensual arribos : " + e.getMessage());
        }
        return dato;
    }

    public void ProduccionTotal() {
        String nombreTabla = Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String dato = "";
        int escama = 0;
        int pulpo = 0;
        int caracol = 0;
        String año = Ver.txt_AÑO.getText();
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT SUM(Produccion_Escama) as totalEscama, SUM(Produccion_Pulpo) as totalPulpo, "
                    + "SUM(Produccion_Caracol) as totalCaracol FROM " + nombreTabla + " WHERE AÑO = " + año;
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                escama = (rs.getInt("totalEscama"));
                pulpo = (rs.getInt("totalPulpo"));
                caracol = (rs.getInt("totalCaracol"));

            }
            rs.close();
            Ver.txt_totalEscama.setText("" + escama + " kg");
            Ver.txt_totalPulpo.setText("" + pulpo + " kg");
            Ver.txt_totalCaracol.setText("" + caracol + " kg");
        } catch (Exception e) {

            Ver.txt_id.setText("");
            Ver.txt_id.requestFocus(true);
            System.err.println("error en buscar arribos total: " + e.getMessage());
        }
    }

    public void IngresoTotal() {
        String nombreTabla = Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String dato = "";
        int escama = 0;
        int pulpo = 0;
        int caracol = 0;
        String año = Ver.txt_AÑO.getText();
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT SUM(TotalEscama) as totalEscama, SUM(TotalPulpo) as totalPulpo, "
                    + "SUM(TotalCaracol) as totalCaracol FROM " + nombreTabla + " WHERE AÑO = " + año;
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                escama = (rs.getInt("totalEscama"));
                pulpo = (rs.getInt("totalPulpo"));
                caracol = (rs.getInt("totalCaracol"));

            }
            rs.close();
            Ver.txt_totalEsc$$.setText("$" + escama);
            Ver.txt_totalPulpo$$$.setText("$" + pulpo);
            Ver.txt_totalCaracol$$$.setText("$" + caracol);
        } catch (Exception e) {

            Ver.txt_id.setText("");
            Ver.txt_id.requestFocus(true);
            System.err.println("error en buscar Suma Anual Ingreso: " + e.getMessage());
        }
    }

    public boolean VericarTabla() {
        String NombreTabla = "Factura_" + Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String nombreSql = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("VerificarTabla ?");
            ps.setString(1, NombreTabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                nombreSql = (rs.getString("NombreTabla"));
            }
            rs.close();
            if (nombreSql.isEmpty()) {
                return true;
            } else {
                System.out.println("nomSQL " + nombreSql);
                return false;
            }

        } catch (Exception e) {
            System.out.println(" Error: Verificar " + e.getMessage());
        }
        return false;
    }

    public String sumaFactura(String nombreTabla, String mes) {
        String dato = "";
        int factMes = 0;
        int dia = 0;
        String año = Ver.txt_AÑO.getText();
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql4 = "";
            String sql = "SELECT SUM(Monto) as Produccion FROM " + nombreTabla + "  WHERE MES = ? AND AÑO = ? AND Estado = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, mes);
            ps.setString(2, año);
            ps.setString(3, "Activo");
            rs = ps.executeQuery();
            while (rs.next()) {
                factMes = (rs.getInt("Produccion"));
                if (factMes == 0) {
                    dato = "Sin Facturar";
                } else {
                    dato = "$ " + factMes;
                }
            }
            rs.close();

        } catch (Exception e) {
            System.err.println("errorFactura " + e.getMessage());
        }
        return dato;
    }

    public String TotalFactura() {
        String nombreTabla = "Factura_" + Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String dato = "";
        String año = Ver.txt_AÑO.getText();
        int factMes = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT SUM(Monto) as ProduccionFactura FROM " + nombreTabla + " WHERE AÑO = " + año + " AND Estado = 'Activo'";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                factMes = (rs.getInt("ProduccionFactura"));
                if (factMes == 0) {
                    dato = "Sin Facturar";
                    Ver.txt_TotalFact.setText(dato);
                } else {
                    dato = "$" + factMes;
                    Ver.txt_TotalFact.setText(dato);
                }
            }
            rs.close();

        } catch (Exception e) {
            System.out.println("error totalFactura " + e.getMessage());
        }
        return dato;
    }

    public void produccionEscama() {
        String nombreTabla = Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String ene = "", feb = "", mar = "", abr = "", may = "", jun = "", jul = "", ago = "", sep = "", oct = "", nov = "", dic = "";
        ene = buscarProdEscama(nombreTabla, "ENE", "sumaEscama");
        Ver.txt_eneEsc.setText("" + ene);
        feb = buscarProdEscama(nombreTabla, "FEB", "sumaEscama");
        Ver.txt_febEsc.setText("" + feb);
        mar = buscarProdEscama(nombreTabla, "MAR", "sumaEscama");
        Ver.txt_marEsc.setText("" + mar);
        abr = buscarProdEscama(nombreTabla, "ABR", "sumaEscama");
        Ver.txt_abrEsc.setText("" + abr);
        may = buscarProdEscama(nombreTabla, "MAY", "sumaEscama");
        Ver.txt_mayEsc.setText("" + may);
        jun = buscarProdEscama(nombreTabla, "JUNE", "sumaEscama");
        Ver.txt_junEsc.setText("" + jun);
        jul = buscarProdEscama(nombreTabla, "JULY", "sumaEscama");
        Ver.txt_julEsc.setText("" + jul);
        ago = buscarProdEscama(nombreTabla, "AGOST", "sumaEscama");
        Ver.txt_agoEsc.setText("" + ago);
        sep = buscarProdEscama(nombreTabla, "SEP", "sumaEscama");
        Ver.txt_sepEsc.setText("" + sep);
        oct = buscarProdEscama(nombreTabla, "OCT", "sumaEscama");
        Ver.txt_octEsc.setText("" + oct);
        nov = buscarProdEscama(nombreTabla, "NOV", "sumaEscama");
        Ver.txt_novEsc.setText("" + nov);
        dic = buscarProdEscama(nombreTabla, "DIC", "sumaEscama");
        Ver.txt_dicEsc.setText("" + dic);
    }

    public void produccionPulpo() {
        String nombreTabla = Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String ago = "", sep = "", oct = "", nov = "", dic = "";

        ago = buscarProdPulpo(nombreTabla, "AGOST", "sumaPulpo");
        Ver.txt_agopul.setText("" + ago);
        sep = buscarProdPulpo(nombreTabla, "SEP", "sumaPulpo");
        Ver.txt_seppul.setText("" + sep);
        oct = buscarProdPulpo(nombreTabla, "OCT", "sumaPulpo");
        Ver.txt_octpul.setText("" + oct);
        nov = buscarProdPulpo(nombreTabla, "NOV", "sumaPulpo");
        Ver.txt_novpul.setText("" + nov);
        dic = buscarProdPulpo(nombreTabla, "DIC", "sumaPulpo");
        Ver.txt_dicpul.setText("" + dic);
    }

    public void produccionCaracol() {
        String nombreTabla = Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String ago = "", sep = "", oct = "", nov = "", dic = "";

        ago = buscarProdCaracol(nombreTabla, "MAR", "sumaCaracol");
        Ver.txt_marcara.setText("" + ago);
        sep = buscarProdCaracol(nombreTabla, "ABR", "sumaCaracol");
        Ver.txt_abrcara.setText("" + sep);
        oct = buscarProdCaracol(nombreTabla, "MAY", "sumaCaracol");
        Ver.txt_maycara.setText("" + oct);
        nov = buscarProdCaracol(nombreTabla, "JUNE", "sumaCaracol");
        Ver.txt_juncara.setText("" + nov);
        dic = buscarProdCaracol(nombreTabla, "JULY", "sumaCaracol");
        Ver.txt_julcara.setText("" + dic);
    }

    public void Facturas() {
        String nombreTabla = "Factura_" + Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String ene = "", feb = "", mar = "", abr = "", may = "", jun = "", jul = "", ago = "", sep = "", oct = "", nov = "", dic = "";

        ene = sumaFactura(nombreTabla, "ENE");
        Ver.FAC_ENE.setText(ene);
        feb = sumaFactura(nombreTabla, "FEB");
        Ver.FAC_FEB.setText(feb);
        mar = sumaFactura(nombreTabla, "MAR");
        Ver.FAC_MAR.setText(mar);
        abr = sumaFactura(nombreTabla, "ABR");
        Ver.FAC_ABRI.setText(abr);
        may = sumaFactura(nombreTabla, "MAY");
        Ver.FAC_MAY.setText(may);
        jun = sumaFactura(nombreTabla, "JUNE");
        Ver.FAC_JUN.setText(jun);
        jul = sumaFactura(nombreTabla, "JULY");
        Ver.FAC_JUL.setText(jul);
        ago = sumaFactura(nombreTabla, "AGOST");
        Ver.FAC_AGO.setText(ago);
        sep = sumaFactura(nombreTabla, "SEP");
        Ver.FAC_SEP.setText(sep);
        oct = sumaFactura(nombreTabla, "OCT");
        Ver.FAC_OCT.setText(oct);
        nov = sumaFactura(nombreTabla, "NOV");
        Ver.FAC_NOV.setText(nov);
        dic = sumaFactura(nombreTabla, "DIC");
        Ver.FAC_DIC.setText(dic);

    }

    public void limpiar() {
        Ver.txt_marcara.setText("");
        Ver.txt_abrcara.setText("");
        Ver.txt_maycara.setText("");
        Ver.txt_juncara.setText("");
        Ver.txt_julcara.setText("");
        Ver.txt_eneEsc.setText("");
        Ver.txt_febEsc.setText("");
        Ver.txt_marEsc.setText("");
        Ver.txt_abrEsc.setText("");
        Ver.txt_mayEsc.setText("");
        Ver.txt_junEsc.setText("");
        Ver.txt_julEsc.setText("");
        Ver.txt_agoEsc.setText("");
        Ver.txt_sepEsc.setText("");
        Ver.txt_octEsc.setText("");
        Ver.txt_novEsc.setText("");
        Ver.txt_dicEsc.setText("");
        Ver.txt_agopul.setText("");
        Ver.txt_seppul.setText("");
        Ver.txt_octpul.setText("");
        Ver.txt_novpul.setText("");
        Ver.txt_dicpul.setText("");
        Ver.FAC_ENE.setText("");
        Ver.FAC_FEB.setText("");
        Ver.FAC_MAR.setText("");
        Ver.FAC_ABRI.setText("");
        Ver.FAC_MAY.setText("");
        Ver.FAC_JUN.setText("");
        Ver.FAC_JUL.setText("");
        Ver.FAC_AGO.setText("");
        Ver.FAC_SEP.setText("");
        Ver.FAC_OCT.setText("");
        Ver.FAC_NOV.setText("");
        Ver.FAC_DIC.setText("");
        Ver.txt_TotalFact.setText("");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == Ver.btn_imprimir) {
            //  Rectangle tamaño = new Rectangle(350,1000);
            //    Document documento = new Document(tamaño);
//        PrinterJob trabajo = PrinterJob.getPrinterJob();
//        if(trabajo.printDialog()){
//            try {
//                trabajo.print();
//            } catch (PrinterException ex) {
//                Logger.getLogger(ControladorResumen.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }else{
//            
//        }
            GuardarResumenArribos();
            Document documento = new Document();
            PreparedStatement ps;
            ResultSet rs;
            PreparedStatement ps2;
            ResultSet rs2;
            PreparedStatement ps3;
            ResultSet rs3;
            PreparedStatement ps4;
            ResultSet rs4;
            LocalDateTime dia = LocalDateTime.now();
            String Año = Ver.txt_AÑO.getText();
            DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
            String fecha = dia.format(formato);
            String nombreArchibo = Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
            String estado = "Activo";

            try {
                String nombre = "ConcentradoArribos_" + Año + "_" + nombreArchibo;
                String ruta = System.getProperty("user.home");

                PdfWriter.getInstance(documento, new FileOutputStream(ruta + "/OneDrive/Escritorio/" + Año + "_ConcentradoArribos/" + nombreArchibo + "/" + nombre.trim() + ".pdf"));

               // com.itextpdf.text.Image header = com.itextpdf.text.Image.getInstance(ruta + "\\OneDrive\\Documentos\\NetBeansProjects\\concentrado-de-arribos\\ConcentradoArribos\\src\\Img\\cabecera.jpg");
                Image header = Image.getInstance("src\\Img\\cabecera.jpg");
                header.scaleToFit(450, 120);
                header.setAlignment(Chunk.ALIGN_CENTER);

                Paragraph parrafo2 = new Paragraph();
                parrafo2.setAlignment(Paragraph.ALIGN_CENTER);
                parrafo2.add(nombreArchibo + "\n"
                        + "ARRIBOS " + Año + "\n \n");
                parrafo2.setFont(FontFactory.getFont("Comic Sans MS", 14, Font.BOLD, BaseColor.DARK_GRAY));

                documento.open();
                documento.add(header);
                documento.add(parrafo2);

                PdfPTable tablaNum = new PdfPTable(4);
                tablaNum.getDefaultCell().setBorder(0);
                tablaNum.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                tablaNum.addCell("MES");
                tablaNum.addCell("ESCAMA");
                tablaNum.addCell("PULPO");
                tablaNum.addCell("CARACOL");
                tablaNum.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

                String[] mes = {"ENE", "FEB", "MAR", "ABR", "MAY", "JUNE", "JULY", "AGOST", "SEP", "OCT", "NOV", "DIC"};
                try {
                    String dato = "";
                    int prodMes = 0;

                    Connection con2 = Conexion.establecerConnection();
                    Connection con3 = Conexion.establecerConnection();
                    Connection con = Conexion.establecerConnection();

                    for (int i = 0; i < mes.length; i++) {

                        String sql = "SELECT SUM(Produccion_Escama) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreArchibo
                                + " WHERE MES = ? and Especie <> 'PULPO' and Especie <> 'Caracol' AND AÑO = ?";

                        ps = con.prepareStatement(sql);
                        ps.setString(1, mes[i]);
                        ps.setString(2, Año);
                        rs = ps.executeQuery();

                        String sql2 = "SELECT SUM(Produccion_Pulpo) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreArchibo
                                + " WHERE MES = ? and Especie = 'PULPO'  AND AÑO = ?";
                        ps2 = con2.prepareStatement(sql2);
                        ps2.setString(1, mes[i]);
                        ps2.setString(2, Año);
                        rs2 = ps2.executeQuery();

                        String sql3 = "SELECT SUM(Produccion_Caracol) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreArchibo + " WHERE MES = ? and Especie = 'Caracol'  AND AÑO = ?";
                        ps3 = con3.prepareStatement(sql3);
                        ps3.setString(1, mes[i]);
                        ps3.setString(2, Año);
                        rs3 = ps3.executeQuery();

                        while (rs.next() && rs2.next() && rs3.next()) {
                            tablaNum.addCell(mes[i]);
                            int prod = rs.getInt("Produccion");
                            if (prod == 0) {
                                tablaNum.addCell("Sin Produccion");
                            } else {
                                tablaNum.addCell("" + rs.getInt("Produccion") + " en " + rs.getInt("dias") + " dias");
                            }
                            /////*************** pulpo
                            if (i < 7) {
                                tablaNum.addCell("VEDA");
                            } else {
                                int prod2 = rs2.getInt("Produccion");
                                if (prod2 == 0) {
                                    tablaNum.addCell("Sin Produccion");
                                } else {
                                    tablaNum.addCell("" + rs2.getInt("Produccion") + " en " + rs2.getInt("dias") + " dias");
                                }
                            }
                            ///////*********** caracol
                            int prod3 = rs3.getInt("Produccion");
                            if (i < 2 || i > 6) {
                                tablaNum.addCell("VEDA");
                            } else if (prod3 == 0) {
                                tablaNum.addCell("Sin Produccion");
                            } else {
                                tablaNum.addCell("" + rs3.getInt("Produccion") + " en " + rs3.getInt("dias") + " dias");
                            }

                        }
                        rs.close();
                        rs2.close();
                        rs3.close();

                    }
                    documento.add(tablaNum);

                } catch (SQLException eq) {
                    System.err.print("Error en obtener Produccion. " + eq.getMessage());
                }

                Paragraph parrafo3 = new Paragraph();
                parrafo3.setAlignment(Paragraph.ALIGN_CENTER);
                parrafo3.add("\n"
                        + "FACTURAS " + Año + "\n \n");
                parrafo3.setFont(FontFactory.getFont("Tahoma", 14, Font.BOLD, BaseColor.DARK_GRAY));

                documento.add(parrafo3);

                PdfPTable tablaFac = new PdfPTable(2);
                tablaFac.getDefaultCell().setBorder(0);
                tablaFac.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                tablaFac.addCell("MES");
                tablaFac.addCell("$FACTURA MESUAL$");
                tablaFac.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
                try {
                    Connection con4 = Conexion.establecerConnection();
                    String nombreFactura = "Factura_" + Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
                    for (int i = 0; i < mes.length; i++) {
                        String sql4 = "SELECT SUM(Monto) as Produccion FROM " + nombreFactura + "  WHERE MES = ? AND AÑO = ? AND Estado = ?";
                        ps4 = con4.prepareStatement(sql4);
                        ps4.setString(1, mes[i]);
                        ps4.setString(2, Año);
                        ps4.setString(3, estado);
                        rs4 = ps4.executeQuery();

                        while (rs4.next()) {
                            tablaFac.addCell(mes[i]);
                            int fac = rs4.getInt("Produccion");
                            if (fac == 0) {
                                tablaFac.addCell("Sin Facturar");
                            } else {
                                tablaFac.addCell("$" + rs4.getInt("Produccion"));
                            }
                        }
                    }
                    documento.add(tablaFac);

                } catch (Exception ef) {
                }

                Paragraph parrafo4 = new Paragraph();
                parrafo4.setAlignment(Paragraph.ALIGN_CENTER);
                parrafo4.add("\n"
                        + "TOTALES " + Año + "\n \n");
                parrafo4.setFont(FontFactory.getFont("Tahoma", 14, Font.BOLD, BaseColor.DARK_GRAY));

                documento.add(parrafo4);

                PdfPTable tablaTotal = new PdfPTable(7);
                tablaTotal.getDefaultCell().setBorder(0);
                tablaTotal.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                tablaTotal.addCell("KG Escama");
                tablaTotal.addCell("$Escama$");
                tablaTotal.addCell("KG Pulpo");
                tablaTotal.addCell("$Pulpo$");
                tablaTotal.addCell("KG Caracol");
                tablaTotal.addCell("$Caracol$");
                tablaTotal.addCell("$Total Facturas$");
                tablaTotal.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

                try {
                    String nombreFactura = "Factura_" + Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
                    if (VericarTabla()) {
//
                        Connection con2 = Conexion.establecerConnection();
                        String sql2 = "SELECT SUM(TotalEscama) as Escama, SUM(TotalPulpo) as Pulpo, "
                                + "SUM(TotalCaracol) as Caracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                        ps2 = con2.prepareStatement(sql2);
                        rs2 = ps2.executeQuery();

                        Connection con3 = Conexion.establecerConnection();
                        String sql3 = "SELECT SUM(Produccion_Escama) as totalEscama, SUM(Produccion_Pulpo) as totalPulpo, "
                                + "SUM(Produccion_Caracol) as totalCaracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                        ps3 = con3.prepareStatement(sql3);
                        rs3 = ps3.executeQuery();

                        while (rs2.next() && rs3.next()) {

                            int escama = (rs2.getInt("Escama"));
                            int pulpo = (rs2.getInt("Pulpo"));
                            int caracol = (rs2.getInt("Caracol"));
                            int escamaProd = (rs3.getInt("totalEscama"));
                            int pulpoProd = (rs3.getInt("totalPulpo"));
                            int caracolProd = (rs3.getInt("totalCaracol"));

                            tablaTotal.addCell("" + escamaProd + " KG");
                            tablaTotal.addCell("$ " + escama);
                            tablaTotal.addCell("" + pulpoProd + " KG");
                            tablaTotal.addCell("$ " + pulpo);
                            tablaTotal.addCell("" + caracolProd + " KG");
                            tablaTotal.addCell("$ " + caracol);
                            tablaTotal.addCell("Sin Facturar");

                        }
                        documento.add(tablaTotal);
                    } else {///////////// si se encuentra en la base de datos
                        Connection con = Conexion.establecerConnection();
                        String sql = "SELECT SUM(Monto) as ProduccionFactura FROM " + nombreFactura + " WHERE AÑO = " + Año + " AND Estado = ?";
                        ps = con.prepareStatement(sql);
                        ps.setString(1, estado);
                        rs = ps.executeQuery();
//
                        Connection con2 = Conexion.establecerConnection();
                        String sql2 = "SELECT SUM(TotalEscama) as Escama, SUM(TotalPulpo) as Pulpo, "
                                + "SUM(TotalCaracol) as Caracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                        ps2 = con2.prepareStatement(sql2);
                        rs2 = ps2.executeQuery();

                        Connection con3 = Conexion.establecerConnection();
                        String sql3 = "SELECT SUM(Produccion_Escama) as totalEscama, SUM(Produccion_Pulpo) as totalPulpo, "
                                + "SUM(Produccion_Caracol) as totalCaracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                        ps3 = con3.prepareStatement(sql3);
                        rs3 = ps3.executeQuery();

                        while (rs.next() && rs2.next() && rs3.next()) {

                            int escama = (rs2.getInt("Escama"));
                            int pulpo = (rs2.getInt("Pulpo"));
                            int caracol = (rs2.getInt("Caracol"));
                            int escamaProd = (rs3.getInt("totalEscama"));
                            int pulpoProd = (rs3.getInt("totalPulpo"));
                            int caracolProd = (rs3.getInt("totalCaracol"));

                            tablaTotal.addCell("" + escamaProd + " KG");
                            tablaTotal.addCell("$ " + escama);
                            tablaTotal.addCell("" + pulpoProd + " KG");
                            tablaTotal.addCell("$ " + pulpo);
                            tablaTotal.addCell("" + caracolProd + " KG");
                            tablaTotal.addCell("$ " + caracol);

                            int factMes = (rs.getInt("ProduccionFactura"));
                            if (factMes == 0) {
                                tablaTotal.addCell("Sin Facturar");
                            } else {
                                tablaTotal.addCell("$ " + rs.getString("ProduccionFactura"));
                            }
                        }
                        documento.add(tablaTotal);
                    }

                } catch (Exception et) {
                    System.out.println("error en total " + et.getMessage());
                }

                documento.close();
                // JOptionPane.showMessageDialog(null, "Reporte creado correctamente.");

            } catch (DocumentException | IOException ed) {
                // JOptionPane.showMessageDialog(null, "Error al generar PDF, contacte al administrador " + ed.getMessage());
            }

        }

    }

    public void GuardarResumenArribos() {
        Document documento = new Document();
        PreparedStatement ps;
        ResultSet rs;
        PreparedStatement ps2;
        ResultSet rs2;
        PreparedStatement ps3;
        ResultSet rs3;
        PreparedStatement ps4;
        ResultSet rs4;
        LocalDateTime dia = LocalDateTime.now();
        String Año = Ver.txt_AÑO.getText();
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        String fecha = dia.format(formato);
        String nombreArchibo = Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
        String estado = "Activo";
        try {
            String nombre = "ConcentradoArribos_" + Año + "_" + nombreArchibo;
            String ruta = System.getProperty("user.home");

            PdfWriter.getInstance(documento, new FileOutputStream(ruta + "/OneDrive/Escritorio/ResumenArribos/" + nombreArchibo + "/" + nombre.trim() + ".pdf"));

        //    com.itextpdf.text.Image header = com.itextpdf.text.Image.getInstance(ruta + "\\OneDrive\\Documentos\\NetBeansProjects\\concentrado-de-arribos\\ConcentradoArribos\\src\\Img\\cabecera.jpg");
            Image header = Image.getInstance("src\\Img\\cabecera.jpg");
            header.scaleToFit(450, 120);
            header.setAlignment(Chunk.ALIGN_CENTER);

            Paragraph parrafo2 = new Paragraph();
            parrafo2.setAlignment(Paragraph.ALIGN_CENTER);
            parrafo2.add(nombreArchibo + "\n"
                    + "ARRIBOS " + Año + "\n \n");
            parrafo2.setFont(FontFactory.getFont("Tahoma", 14, Font.BOLD, BaseColor.DARK_GRAY));

            documento.open();
            documento.add(header);
            documento.add(parrafo2);

            PdfPTable tablaNum = new PdfPTable(4);
             tablaNum.getDefaultCell().setBorder(0);
                tablaNum.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                tablaNum.addCell("MES");
                tablaNum.addCell("ESCAMA");
                tablaNum.addCell("PULPO");
                tablaNum.addCell("CARACOL");
                tablaNum.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

            String[] mes = {"ENE", "FEB", "MAR", "ABR", "MAY", "JUNE", "JULY", "AGOST", "SEP", "OCT", "NOV", "DIC"};
            try {
                String dato = "";
                int prodMes = 0;

                Connection con2 = Conexion.establecerConnection();
                Connection con3 = Conexion.establecerConnection();
                Connection con = Conexion.establecerConnection();

                for (int i = 0; i < mes.length; i++) {

                    String sql = "SELECT SUM(Produccion_Escama) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreArchibo
                            + " WHERE MES = ? and Especie <> 'PULPO' and Especie <> 'Caracol' AND AÑO = ?";

                    ps = con.prepareStatement(sql);
                    ps.setString(1, mes[i]);
                    ps.setString(2, Año);
                    rs = ps.executeQuery();

                    String sql2 = "SELECT SUM(Produccion_Pulpo) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreArchibo
                            + " WHERE MES = ? and Especie = 'PULPO'  AND AÑO = ?";
                    ps2 = con2.prepareStatement(sql2);
                    ps2.setString(1, mes[i]);
                    ps2.setString(2, Año);
                    rs2 = ps2.executeQuery();

                    String sql3 = "SELECT SUM(Produccion_Caracol) as Produccion, SUM(DiasDe_Pesca) as dias FROM " + nombreArchibo + " WHERE MES = ? and Especie = 'Caracol'  AND AÑO = ?";
                    ps3 = con3.prepareStatement(sql3);
                    ps3.setString(1, mes[i]);
                    ps3.setString(2, Año);
                    rs3 = ps3.executeQuery();

                    while (rs.next() && rs2.next() && rs3.next()) {
                        tablaNum.addCell(mes[i]);
                        int prod = rs.getInt("Produccion");
                        if (prod == 0) {
                            tablaNum.addCell("Sin Produccion");
                        } else {
                            tablaNum.addCell("" + rs.getInt("Produccion") + " en " + rs.getInt("dias") + " dias");
                        }
                        /////*************** pulpo
                        if (i < 7) {
                            tablaNum.addCell("VEDA");
                        } else {
                            int prod2 = rs2.getInt("Produccion");
                            if (prod2 == 0) {
                                tablaNum.addCell("Sin Produccion");
                            } else {
                                tablaNum.addCell("" + rs2.getInt("Produccion") + " en " + rs2.getInt("dias") + " dias");
                            }
                        }
                        ///////*********** caracol
                        int prod3 = rs3.getInt("Produccion");
                        if (i < 2 || i > 6) {
                            tablaNum.addCell("VEDA");
                        } else if (prod3 == 0) {
                            tablaNum.addCell("Sin Produccion");
                        } else {
                            tablaNum.addCell("" + rs3.getInt("Produccion") + " en " + rs3.getInt("dias") + " dias");
                        }

                    }
                    rs.close();
                    rs2.close();
                    rs3.close();

                }
                documento.add(tablaNum);

            } catch (SQLException eq) {
                System.err.print("Error en obtener Produccion. " + eq.getMessage());
            }

            Paragraph parrafo3 = new Paragraph();
            parrafo3.setAlignment(Paragraph.ALIGN_CENTER);
            parrafo3.add("\n"
                    + "FACTURAS " + Año + "\n \n");
            parrafo3.setFont(FontFactory.getFont("Tahoma", 14, Font.BOLD, BaseColor.DARK_GRAY));

            documento.add(parrafo3);

            PdfPTable tablaFac = new PdfPTable(2);
            tablaFac.getDefaultCell().setBorder(0);
                tablaFac.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                tablaFac.addCell("MES");
                tablaFac.addCell("$FACTURA MESUAL$");
                tablaFac.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

            try {
                Connection con4 = Conexion.establecerConnection();
                String nombreFactura = "Factura_" + Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
                for (int i = 0; i < mes.length; i++) {
                    String sql4 = "SELECT SUM(Monto) as Produccion FROM " + nombreFactura + "  WHERE MES = ? AND AÑO = ? AND Estado = ?";
                    ps4 = con4.prepareStatement(sql4);
                    ps4.setString(1, mes[i]);
                    ps4.setString(2, Año);
                    ps4.setString(3, estado);
                    rs4 = ps4.executeQuery();

                    while (rs4.next()) {
                        tablaFac.addCell(mes[i]);
                        int fac = rs4.getInt("Produccion");
                        if (fac == 0) {
                            tablaFac.addCell("Sin Facturar");
                        } else {
                            tablaFac.addCell("$" + rs4.getInt("Produccion"));
                        }
                    }
                }
                documento.add(tablaFac);

            } catch (Exception ef) {
            }

            Paragraph parrafo4 = new Paragraph();
            parrafo4.setAlignment(Paragraph.ALIGN_CENTER);
            parrafo4.add("\n"
                    + "TOTALES " + Año + "\n \n");
            parrafo4.setFont(FontFactory.getFont("Tahoma", 14, Font.BOLD, BaseColor.DARK_GRAY));

            documento.add(parrafo4);

            PdfPTable tablaTotal = new PdfPTable(7);
            tablaTotal.getDefaultCell().setBorder(0);
                tablaTotal.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
                tablaTotal.addCell("KG Escama");
                tablaTotal.addCell("$Escama$");
                tablaTotal.addCell("KG Pulpo");
                tablaTotal.addCell("$Pulpo$");
                tablaTotal.addCell("KG Caracol");
                tablaTotal.addCell("$Caracol$");
                tablaTotal.addCell("$Total Facturas$");
                tablaTotal.getDefaultCell().setBackgroundColor(BaseColor.WHITE);

            try {
                String nombreFactura = "Factura_" + Ver.txt_NombreArribo.getText().replace(" ", "") + "_" + Ver.txt_Apellido.getText().replace(" ", "");
                if (VericarTabla()) {
//
                    Connection con2 = Conexion.establecerConnection();
                    String sql2 = "SELECT SUM(TotalEscama) as Escama, SUM(TotalPulpo) as Pulpo, "
                            + "SUM(TotalCaracol) as Caracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                    ps2 = con2.prepareStatement(sql2);
                    rs2 = ps2.executeQuery();

                    Connection con3 = Conexion.establecerConnection();
                    String sql3 = "SELECT SUM(Produccion_Escama) as totalEscama, SUM(Produccion_Pulpo) as totalPulpo, "
                            + "SUM(Produccion_Caracol) as totalCaracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                    ps3 = con3.prepareStatement(sql3);
                    rs3 = ps3.executeQuery();

                    while (rs2.next() && rs3.next()) {

                        int escama = (rs2.getInt("Escama"));
                        int pulpo = (rs2.getInt("Pulpo"));
                        int caracol = (rs2.getInt("Caracol"));
                        int escamaProd = (rs3.getInt("totalEscama"));
                        int pulpoProd = (rs3.getInt("totalPulpo"));
                        int caracolProd = (rs3.getInt("totalCaracol"));

                        tablaTotal.addCell("" + escamaProd + " KG");
                        tablaTotal.addCell("$ " + escama);
                        tablaTotal.addCell("" + pulpoProd + " KG");
                        tablaTotal.addCell("$ " + pulpo);
                        tablaTotal.addCell("" + caracolProd + " KG");
                        tablaTotal.addCell("$ " + caracol);
                        tablaTotal.addCell("Sin Facturar");

                    }
                    documento.add(tablaTotal);
                } else {///////////// si se encuentra en la base de datos
                    Connection con = Conexion.establecerConnection();
                    String sql = "SELECT SUM(Monto) as ProduccionFactura FROM " + nombreFactura + " WHERE AÑO = " + Año + " AND Estado = ?";
                    ps = con.prepareStatement(sql);
                    ps.setString(1, estado);
                    rs = ps.executeQuery();
//
                    Connection con2 = Conexion.establecerConnection();
                    String sql2 = "SELECT SUM(TotalEscama) as Escama, SUM(TotalPulpo) as Pulpo, "
                            + "SUM(TotalCaracol) as Caracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                    ps2 = con2.prepareStatement(sql2);
                    rs2 = ps2.executeQuery();

                    Connection con3 = Conexion.establecerConnection();
                    String sql3 = "SELECT SUM(Produccion_Escama) as totalEscama, SUM(Produccion_Pulpo) as totalPulpo, "
                            + "SUM(Produccion_Caracol) as totalCaracol FROM " + nombreArchibo + " WHERE AÑO = " + Año;
                    ps3 = con3.prepareStatement(sql3);
                    rs3 = ps3.executeQuery();

                    while (rs.next() && rs2.next() && rs3.next()) {

                        int escama = (rs2.getInt("Escama"));
                        int pulpo = (rs2.getInt("Pulpo"));
                        int caracol = (rs2.getInt("Caracol"));
                        int escamaProd = (rs3.getInt("totalEscama"));
                        int pulpoProd = (rs3.getInt("totalPulpo"));
                        int caracolProd = (rs3.getInt("totalCaracol"));

                        tablaTotal.addCell("" + escamaProd + " KG");
                        tablaTotal.addCell("$ " + escama);
                        tablaTotal.addCell("" + pulpoProd + " KG");
                        tablaTotal.addCell("$ " + pulpo);
                        tablaTotal.addCell("" + caracolProd + " KG");
                        tablaTotal.addCell("$ " + caracol);

                        int factMes = (rs.getInt("ProduccionFactura"));
                        if (factMes == 0) {
                            tablaTotal.addCell("Sin Facturar");
                        } else {
                            tablaTotal.addCell("$ " + rs.getString("ProduccionFactura"));
                        }
                    }
                    documento.add(tablaTotal);
                }

            } catch (Exception et) {
                System.out.println("error en total " + et.getMessage());
            }

            documento.close();
            JOptionPane.showMessageDialog(null, "Reporte creado correctamente.");

        } catch (DocumentException | IOException ed) {
            JOptionPane.showMessageDialog(null, "Error al generar PDF, contacte al administrador " + ed.getMessage());
        }
    }

}
