/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.IngresarArribos;
import Vista.IngresarFactClientes;
import Vista.IngresarFacturas;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorFacturas implements ActionListener, KeyListener, MouseListener {

    IngresarFacturas arr;
    int id, Num_Embarcacion;
    String Nombre, Apellido, Contraseña, Nombre_Embarcacion, RNPA, PermisoEscama, PermisoPulpo, PermisoCaracol,
            FechaInicioPermiso, FechaFinPermiso, FechaFielInicio, FechaFielFin, Estado;

    //CONTRUCTOR, EN DONDE SE INICIALIZA TODOS LO EVENTTOS
    public ControladorFacturas(IngresarFacturas arr) {
        this.arr = arr;
        this.arr.txt_id.addKeyListener(this);
        this.arr.cbx_Especie.addKeyListener(this);
        arr.btnModificar.addActionListener(this);
        arr.btn_AceptarIngreso.addActionListener(this);
        arr.btn_registrarArribo.addActionListener(this);
        arr.btn_registrarArribo.addMouseListener(this);
        this.arr.tb_Permisionario.addMouseListener(this);
        this.arr.btn_nuevo.addActionListener(this);
        this.arr.txt_Contraseña.addKeyListener(this);
        this.arr.btn_registrarArribo.addKeyListener(this);
        this.arr.tb_Permisionario2.addMouseListener(this);
        this.arr.txt_Mes.addKeyListener(this);
        this.arr.btn_CancelarFactura.addActionListener(this);
        this.arr.btn_eliminar.addActionListener(this);
        this.arr.txt_AÑO.addKeyListener(this);
        this.arr.txt_Factura.addKeyListener(this);
        this.arr.btnModificar.addKeyListener(this);
        Botones(false, false, false);
//        buscarIdFactura();
        LLenarCBX();

    }

// ******************** METODO EN DONDE RECIBE LOE EVENTOS DE BOTONES EL LA VISTA HACER ARRIBOS
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == arr.btn_registrarArribo) {
            Ingresar();
            Botones(true, true, true);
            Limpiar();
            cargarTablaFacturas();

        } else if (e.getSource() == arr.btn_AceptarIngreso) {
            if (arr.txt_Folio.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Digite el Folio del arribo", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            } else {
                TerminarRegistro();
                Botones(false, false, true);
                ActualizarId();
                Limpiar();
                cargarTablaFacturas();
                limpiarTable();
                limpiarTableFactura();
                arr.txt_id.requestFocus(true);
            }
        } else if (e.getSource() == arr.btnModificar) {
            if (arr.txt_id.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Dijite el id del Arribo", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            } else {
                Modificar();
                cargarTablaFacturas();

            }
        } else if (e.getSource() == arr.btn_nuevo) {
            Limpiar();
            ActualizarId();
            buscarIdFactura();
            arr.cbx_Especie.requestFocus(true);
            Botones(false, false, true);
        } else if (e.getSource() == arr.btn_CancelarFactura) {
            Eliminar();
        } else if (e.getSource() == arr.btn_eliminar) {
            EliminarRegistro();
        }
    }

//****** LIMPIA TODOS LOS ESPACIOS EN DONDE SE EDITO
    public void Limpiar() {
        arr.txt_Folio.setText("");
        arr.txt_ProdEscama.setText("");
        arr.txt_ProdPulpo.setText("");
        arr.txt_ProdCaracol.setText("");
        arr.txt_montoFact.setText("");
        arr.txt_Facturado.setText("");
        arr.txt_Factura.setText("");
        arr.txt_Leyenda.setText("");

    }
    
    public void TerminarRegistro(){
        
        id = Integer.parseInt(arr.txt_idFac.getText());
        String folio = arr.txt_Factura.getText();
        String nombreTabla = "Factura_" +arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + nombreTabla + " SET Factura = ?  WHERE id_Factura = ?");
            ps.setString(1, folio);
            ps.setInt(2, id);
            ps.executeUpdate();
            cargarTablaFacturas();
            JOptionPane.showMessageDialog(null, "Se finalizo la accion correctamente");
        } catch (Exception e) {
            System.out.println("error modificarFolio " + e.getMessage());
        }
    
    }

    /// ********* MODIFICA LOS BOTONES DE LA VENTANA, PONIENDOLO A DISPOSICION O NO
    public void Botones(boolean ver, boolean ver1, boolean ver2) {
        arr.btnModificar.setEnabled(ver);
        arr.btn_AceptarIngreso.setEnabled(ver1);
        arr.btn_registrarArribo.setEnabled(ver2);
        arr.btn_CancelarFactura.setEnabled(ver);
    }

    public void buscarClientes() {
        int id = Integer.parseInt(arr.txt_id.getText());
        String client = "";
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Clientes FROM ListaFacturas WHERE id = " + id);
            rs = ps.executeQuery();
            while (rs.next()) {
                client = (rs.getString("Clientes"));
                if (client.equals("NO")) {
                } else if (client.equals("SI")) {
                    IngresarFactClientes clienFact = new IngresarFactClientes();
                    clienFact.setVisible(true);
                    clienFact.setTitle("Facturas con Clientes");
                    clienFact.txt_id.setText("" + id);
                    arr.setVisible(false);

                }
            }

        } catch (Exception e) {
        }
    }

    public void Eliminar() {
        id = Integer.parseInt(arr.txt_idArribo.getText());
        String nombreTabla = "Factura_" + arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        String especie = arr.cbx_Especie.getSelectedItem().toString();
        String Factura = arr.txt_Factura.getText();
        String Folio = arr.txt_Folio.getText();
        Estado = "Cancelado";
        int idfac = Integer.parseInt(arr.txt_idFac.getText());
        if (arr.btn_CancelarFactura.getText().equals("Cancelar Factura")) {
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE " + nombreTabla + " SET Estado = ? WHERE id = ? AND Folio = ? AND Especie = ? AND Factura = ? AND id_Factura = ?");
                ps.setString(1, Estado);
                ps.setInt(2, id);
                ps.setString(3, Folio);
                ps.setString(4, especie);
                ps.setString(5, Factura);
                ps.setInt(6, idfac);
                ps.executeUpdate();
                cargarTablaFacturas();
                JOptionPane.showMessageDialog(null, "Se Elimino al Permisionario correctamente");
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        } else if (arr.btn_CancelarFactura.getText().equals("Reingresar Factura")) {
            String Estado2 = "Activo";
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps2 = conectar.prepareStatement("UPDATE " + nombreTabla + " SET Estado = ? WHERE id = ? AND Folio = ? AND Especie = ? AND Factura = ?");
                ps2.setString(1, Estado2);
                ps2.setInt(2, id);
                ps2.setString(3, Folio);
                ps2.setString(4, especie);
                ps2.setString(5, Factura);
                ps2.executeUpdate();
                cargarTablaFacturas();
                JOptionPane.showMessageDialog(null, "Se Reingreso al Permisionario correctamente");
            } catch (Exception e) {
                //System.out.println("error: "+e.getMessage());
            }
        }

    }

    //******* ACTUALIZA ID DESDE LA BASE DE DATOS SQL SERVER
    public void ActualizarId() {
        int idAux = 0;
        String nombreTabla = "Factura_"+arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM " + nombreTabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            arr.txt_idArribo.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    public int buscarIdFactura() {
        int idAux = 0;
        String NombreTabla = "Factura_" + arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id_factura) AS id_Maximo FROM " + NombreTabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo"));
            }

            arr.txt_idFac.setText("" + (idAux + 1));
        } catch (Exception e) {
            System.err.println("error ActFolio: " + e.getMessage());
        }
        return idAux;
    }

    public void limpiarTable() {
        DefaultTableModel modeloTabla = (DefaultTableModel) arr.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            modeloTabla.removeRow(i);
            i = i - 1;
        }
    }

    public void limpiarTableFactura() {
        DefaultTableModel modeloTabla = (DefaultTableModel) arr.tb_Permisionario2.getModel();
        modeloTabla.setRowCount(0);
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            modeloTabla.removeRow(i);
            i = i - 1;
        }
    }

    //******** ELIMINA CUALQUIER REGISTRO QUE SE DESEA BORRAR DE LA BASE DE DATOS
    public void EliminarRegistro() {
        id = Integer.parseInt(arr.txt_idArribo.getText());
        String especie = arr.cbx_Especie.getSelectedItem().toString();
        String nombreTabla = "Factura_" + arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        String mes = arr.txt_Mes.getText();
        String Factura = arr.txt_Factura.getText();
        String Folio = arr.txt_Folio.getText();
        int idfac = Integer.parseInt(arr.txt_idFac.getText());
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("DELETE " + nombreTabla + " WHERE id = ? AND Folio = ? AND Especie = ? AND Factura = ? AND id_Factura  = ?");
            ps.setInt(1, id);
            ps.setString(2, Folio);
            ps.setString(3, especie);
            ps.setString(4, Factura);
            ps.setInt(5, idfac);
            ps.executeUpdate();
            cargarTabla(mes);
            cargarTablaFacturas();
            JOptionPane.showMessageDialog(null, "Se Elimino el registro seleccionado correctamente");
        } catch (Exception e) {
            //System.out.println("error: "+e.getMessage());
        }
    }

    //*********** INGRESA LOS AVISOS DE ARRIBOS, ESTE ES LLAMADO EN EL EVENTO DEL BOTON INGRESAR
    public void Ingresar() {
        String año = arr.txt_AÑO.getText();
        String mes = arr.txt_Mes.getText();
        String fecha = arr.txt_fecha.getText();
        id = Integer.parseInt(arr.txt_idArribo.getText());
        String folio = arr.txt_Folio.getText();
        String Especie = arr.cbx_Especie.getSelectedItem().toString();
        int produccion_escama = Integer.parseInt(arr.txt_ProdEscama.getText());
        int produccion_Pulpo = Integer.parseInt(arr.txt_ProdPulpo.getText());
        int produccion_Caracol = Integer.parseInt(arr.txt_ProdCaracol.getText());
        String Factura = arr.txt_Factura.getText();
        String Facturado = arr.txt_Facturado.getText();
        int monto = Integer.parseInt(arr.txt_montoFact.getText());
        String Estado = "Facturado";
        String FacEstado = "Activo";
        String nombreTablaArribo = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        String nombreTabla = "Factura_" + arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        int idfac = Integer.parseInt(arr.txt_idFac.getText());
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO " + nombreTabla
                    + "(AÑO, MES, Fecha,id,id_Factura,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                    + "Factura, Facturado, Monto, Estado ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, año);
            ps.setString(2, mes);
            ps.setString(3, fecha);
            ps.setInt(4, id);
            ps.setInt(5, idfac);
            ps.setString(6, folio);
            ps.setString(7, Especie);
            ps.setInt(8, produccion_escama);
            ps.setInt(9, produccion_Pulpo);
            ps.setInt(10, produccion_Caracol);
            ps.setString(11, Factura);
            ps.setString(12, Facturado);
            ps.setInt(13, monto);
            ps.setString(14, FacEstado);
            ps.executeUpdate();
            cargarTabla(mes);
            JOptionPane.showMessageDialog(null, "Se Ingreso la factura correctamente");

            PreparedStatement ps2 = conectar.prepareStatement("UPDATE " + nombreTablaArribo
                    + " SET Estado = ? WHERE Folio = ? AND Especie = ?");
            ps2.setString(1, Estado);
            ps2.setString(2, folio);
            ps2.setString(3, Especie);
            ps2.executeUpdate();
            JOptionPane.showMessageDialog(null, "Se facturo correctamente");
            cargarTabla(mes);
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    // ****************** MODIFICA LOS ATRIBUTOS DEL AVISO DE ARRIBO EN LA BASE DE DATOS, ESTE ES LLAMADO EN
    //EL EVENTO DEL BOTON MODIFICAR
    public void Modificar() {
        String año = arr.txt_AÑO.getText();
        String mes = arr.txt_Mes.getText();
        String fecha = arr.txt_fecha.getText();
        id = Integer.parseInt(arr.txt_idArribo.getText());
        String folio = arr.txt_Folio.getText();
        String Especie = arr.cbx_Especie.getSelectedItem().toString();
        int produccion_escama = Integer.parseInt(arr.txt_ProdEscama.getText());
        int produccion_Pulpo = Integer.parseInt(arr.txt_ProdPulpo.getText());
        int produccion_Caracol = Integer.parseInt(arr.txt_ProdCaracol.getText());
        String Factura = arr.txt_Factura.getText();
        String Facturado = arr.txt_Facturado.getText();
        int monto = Integer.parseInt(arr.txt_montoFact.getText());
        int idfac = Integer.parseInt(arr.txt_idFac.getText());
        String nombreTabla = "Factura_" + arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + nombreTabla
                    + " SET AÑO = ?, MES = ?, Fecha = ?, Factura = ?, Facturado = ?, Monto = ? "
                    + " WHERE id = ? AND Especie = ? AND id_Factura = ?");
            ps.setString(1, año);
            ps.setString(2, mes);
            ps.setString(3, fecha);
            ps.setString(4, Factura);
            ps.setString(5, Facturado);
            ps.setInt(6, monto);
            ps.setInt(7, id);
            ps.setString(8, Especie);
            ps.setInt(9, idfac);
            ps.executeUpdate();
            cargarTabla(mes);
            cargarTablaFacturas();
            JOptionPane.showMessageDialog(null, "Se Modifico la Factura correctamente");
        } catch (Exception e) {
            System.out.println("error modificar " + e.getMessage());
        }
    }

    //MODIFICA EL FOLIO DEL ARRIBO, DEPENDIENDO DEL ID
    public void ModificarFolio() {

        id = Integer.parseInt(arr.txt_idArribo.getText());
        String folio = arr.txt_Folio.getText();
        String mes = arr.txt_Mes.getText();
        int idfac = Integer.parseInt(arr.txt_idFac.getText());
        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");

        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE " + nombreTabla + " SET Folio = ? WHERE id_Factura = ? AND id = ?");
            ps.setString(1, folio);
            ps.setInt(2, idfac);
            ps.setInt(3, id);
            ps.executeUpdate();
            cargarTabla(mes);
            JOptionPane.showMessageDialog(null, "Se finalizo la accion correctamente");
        } catch (Exception e) {
            System.out.println("error modificarFolio " + e.getMessage());
        }
    }

    public void buscarPermiso() {
        int id = Integer.parseInt(arr.txt_id.getText());
        String Especie = arr.txt_auxEspecie.getText();
        String permisoEscama = "";
        String permisoPulpo = "";
        String permisoCaracol = "";
        String RNPA = "";
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            String consulta = "SELECT RNPA, Permiso_Escama, Permiso_Pulpo, Permiso_Caracol FROM Permisionario WHERE id = " + id;
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                RNPA = rs.getString("RNPA");
                permisoEscama = rs.getString("Permiso_Escama");
                permisoPulpo = rs.getString("Permiso_Pulpo");
                permisoCaracol = rs.getString("Permiso_Caracol");
            }
            if (Especie.equals("CARACOL")) {
                arr.txt_Leyenda.setText("RNPA: " + RNPA + " PERMISO ESCAMA: " + permisoEscama + " PERMISO CARACOL: " + permisoCaracol);
            } else if (Especie.equals("PULPO")) {
                arr.txt_Leyenda.setText("RNPA: " + RNPA + " PERMISO ESCAMA: " + permisoEscama + " PERMISO PULPO: " + permisoPulpo);
            } else if (!(Especie.equals("PULPO") || Especie.equals("CARACOL"))) {
                arr.txt_Leyenda.setText("RNPA: " + RNPA + " PERMISO ESCAMA: " + permisoEscama);
            }

        } catch (SQLException e) {

        }

    }

    //******** CARGA LA TABLA DESDE LA BASE DE DATOS, DE LOS ARRIBOS REGISTRADOS
    public void cargarTabla(String mes) {
        DefaultTableModel modeloTabla = (DefaultTableModel) arr.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        String año = arr.txt_AÑO.getText();
        int columnas;
        int[] ancho = {50, 80, 130, 90, 80, 80, 80, 80, 100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            arr.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {

            Connection con = Conexion.establecerConnection();
            String consulta = "SELECT id, Fecha, Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, Precio, Estado FROM " + nombreTabla + " ORDER BY id ASC";
            String sql = "SELECT id, Fecha, Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, Precio, Estado FROM " + nombreTabla + " WHERE AÑO = " + año + " AND MES LIKE '%" + mes + "%' ORDER BY id ASC";
            if ("".equalsIgnoreCase(mes) || "".equalsIgnoreCase(año)) {
                ps = con.prepareStatement(consulta);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            } else {
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();

                while (rs.next()) {
                    Object[] fila = new Object[columnas];
                    for (int i = 0; i < columnas; i++) {
                        fila[i] = rs.getObject(i + 1);
                    }
                    modeloTabla.addRow(fila);
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n"
                    + "Verifique el id Permisionario", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void cargarTablaFacturas() {
        DefaultTableModel modeloTabla = (DefaultTableModel) arr.tb_Permisionario2.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        String nombreTabla = "Factura_" + arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
        String año = arr.txt_AÑO.getText();
        String mes = arr.txt_Mes.getText();
        int columnas;
        int[] ancho = {80, 80, 120, 100, 180, 70, 80, 80};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            arr.tb_Permisionario2.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id_Factura,Fecha,Folio, Especie, Factura, Facturado, Monto, Estado  FROM " + nombreTabla + " WHERE AÑO =" + año + " AND MES LIKE '%" + mes + "%' ORDER BY id_Factura ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n"
                    + "Verifique el id_Facturas", "Error", JOptionPane.ERROR_MESSAGE);
            System.err.println("error en tabla facturas " + e.getMessage());
        }
    }

    public void LLenarCBX() {
        try {
            arr.cbx_Especie.removeAllItems();
            String estado = "Activo";
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Especie FROM Especies WHERE Estado = ?");
            ps.setString(1, estado);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                String Especie = rs.getString("Especie");
                arr.cbx_Especie.addItem(Especie);
                i++;
            }

        } catch (Exception er) {
            System.err.println("Error en cbx: " + er.toString());
        }

    }

    //NO HACE ND
    public void keyTyped(KeyEvent e) {

    }

    //*****EVENTO DEL TECLADO AL PONERSE EN EL TXT_ID, PARA PODER REALIZAR CIERTAS ACCIONES
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() == arr.txt_id) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                String mes = arr.txt_Mes.getText();
                buscarClientes();
                arr.txt_Contraseña.requestFocus(true);
                ActualizarId();
                cargarTabla(mes);
                buscarIdFactura();
                cargarTablaFacturas();
                arr.txt_montoFact.setText("0");
            }
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == arr.txt_Contraseña) {
                arr.cbx_Especie.requestFocus(true);
                arr.txt_montoFact.setText("0");
                arr.cbx_Especie.requestFocus(true);
                arr.txt_Factura.setText("0");
            } else if (e.getSource() == arr.cbx_Especie) {
                String especie = arr.cbx_Especie.getSelectedItem().toString();
                if (especie.equals("CARACOL")) {
                    arr.txt_ProdEscama.setEnabled(false);
                    arr.txt_ProdEscama.setText("0");
                    arr.txt_ProdPulpo.setText("0");
                    arr.txt_ProdCaracol.setEnabled(true);
                    arr.txt_ProdCaracol.requestFocus(true);
                } else if (especie.equals("PULPO")) {
                    arr.txt_ProdEscama.setEnabled(false);
                    arr.txt_montoFact.setText("0");
                    arr.txt_ProdEscama.setText("0");
                    arr.txt_ProdCaracol.setEnabled(false);
                    arr.txt_ProdPulpo.setEnabled(true);
                    arr.txt_ProdPulpo.requestFocus(true);
                } else if (!(especie.equals("PULPO") || especie.equals("CARACOL"))) {
                    arr.txt_montoFact.setText("0");
                    arr.txt_ProdPulpo.setText("0");
                    arr.txt_ProdEscama.setEnabled(true);
                    arr.txt_ProdPulpo.setEnabled(false);
                    arr.txt_ProdCaracol.setEnabled(false);
                    arr.txt_ProdEscama.requestFocus(true);
                }

            } else if (e.getSource() == arr.btn_registrarArribo) {
                Ingresar();
                String mes = arr.txt_Mes.getText();
                cargarTabla(mes);
                cargarTablaFacturas();
                Limpiar();

            } else if (e.getSource() == arr.txt_Factura) {
                arr.btnModificar.requestFocus();

            } else if (e.getSource() == arr.btnModificar) {
                Modificar();
                cargarTablaFacturas();
            }

        }
    }

    //********** REALIZA UNA COSULTA EN LA BASE DE DATOS PARA LOCALIZAR LOS DATOS DEL PERMISIONARIO
    @Override
    public void keyReleased(KeyEvent e) {
        int textoid = 0;
        if (e.getSource() == arr.txt_id) {
            if (arr.txt_id.getText().isEmpty()) {
                textoid = 0;
                limpiarTable();
                limpiarTableFactura();
                Limpiar();
            } else {
                textoid = Integer.parseInt(arr.txt_id.getText());
            }
            if (e.getSource() == arr.txt_id) {
                try {
                    PreparedStatement ps;
                    ResultSet rs;
                    Connection con = Conexion.establecerConnection();
                    String sql = "SELECT * FROM Permisionario";
                    String consulta = "SELECT *FROM Permisionario WHERE id = " + textoid;
                    if ("".equalsIgnoreCase(arr.txt_id.getText())) {
                        arr.txt_NombreArribo.setText("");
                        arr.txt_Apellido.setText("");
                        arr.txt_Contraseña.setText("");
                        arr.lb_num_embarcacion.setText("");
                        arr.lb_VigPermiso.setText("");
                        arr.lb_VigFiel.setText("");
                    } else {
                        ps = con.prepareStatement(consulta);
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            arr.txt_NombreArribo.setText(rs.getString("Nombre"));
                            arr.txt_Apellido.setText(rs.getString("Apellido"));
                            arr.txt_Contraseña.setText(rs.getString("Contraseña"));
                            arr.lb_num_embarcacion.setText("" + rs.getInt("Num_Embarcacion"));
                            arr.lb_VigPermiso.setText(rs.getString("Fecha_fin_PermisoEscama"));
                            arr.lb_VigFiel.setText(rs.getString("Fecha_fin_Fiel"));
                        }
                        Botones(false, false, true);
                    }

                } catch (Exception ev) {
                    JOptionPane.showMessageDialog(null, "El Permisionario no se encuentra en la base de Datos\n"
                            + "Verifique el id", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (e.getSource() == arr.txt_Mes) {
            String mes = arr.txt_Mes.getText();
            cargarTabla(mes);
            cargarTablaFacturas();
        } else if (e.getSource() == arr.txt_AÑO) {
            String mes = arr.txt_Mes.getText();
            cargarTabla(RNPA);
            cargarTablaFacturas();
        }

    }

    //***** EVENTO QUE AFECTA A LA TABLA, PARA PODER COLOCAR LOS DATOS EN SUS CORRESPONDIENTES TXT DE LA VISTA
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == arr.tb_Permisionario) {
            try {
                int fila = arr.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(arr.tb_Permisionario.getValueAt(fila, 0).toString());
                String especie = arr.tb_Permisionario.getValueAt(fila, 3).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                String nombreTabla = arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
                ps = con.prepareStatement("SELECT AÑO, MES, id, Fecha,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol"
                        + " FROM " + nombreTabla + " WHERE id = ? AND Especie = ?");
                ps.setInt(1, id);
                ps.setString(2, especie);
                rs = ps.executeQuery();
                while (rs.next()) {
                    arr.txt_AÑO.setText(rs.getString("AÑO"));
                    arr.txt_Mes.setText(rs.getString("MES"));
                    arr.txt_idArribo.setText("" + rs.getInt("id"));
                    arr.txt_fecha.setText(rs.getString("Fecha"));
                    arr.txt_Folio.setText(rs.getString("Folio"));
                    arr.txt_auxEspecie.setText(rs.getString("Especie"));
                    arr.cbx_Especie.setSelectedItem(arr.txt_auxEspecie.getText());
                    arr.txt_ProdEscama.setText(rs.getString("Produccion_Escama"));
                    arr.txt_ProdPulpo.setText(rs.getString("Produccion_Pulpo"));
                    arr.txt_ProdCaracol.setText(rs.getString("Produccion_Caracol"));
                }
                buscarPermiso();
                Botones(false, true, true);
                arr.txt_ProdCaracol.setEnabled(true);
                arr.txt_ProdPulpo.setEnabled(true);
                arr.txt_ProdEscama.setEnabled(true);
                arr.txt_Facturado.requestFocus(true);
                arr.txt_Factura.setText("");
                arr.txt_montoFact.setText("0");
                arr.txt_Facturado.setText("");
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        } else if (e.getSource() == arr.tb_Permisionario2) {
            try {
                int fila = arr.tb_Permisionario2.getSelectedRow();
                int id = Integer.parseInt(arr.tb_Permisionario2.getValueAt(fila, 0).toString());
                String estado = arr.tb_Permisionario2.getValueAt(fila, 7).toString();
                String especie = arr.tb_Permisionario2.getValueAt(fila, 3).toString();
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                String nombreTabla = "Factura_" + arr.txt_NombreArribo.getText().replace(" ", "") + "_" + arr.txt_Apellido.getText().replace(" ", "");
                ps = con.prepareStatement("SELECT AÑO, MES, id, id_Factura,Fecha,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, Factura, "
                        + "Facturado, Monto, Estado FROM " + nombreTabla + " WHERE id_Factura = ? AND Especie = ? AND Estado = ?");
                ps.setInt(1, id);
                ps.setString(2, especie);
                ps.setString(3, estado);
                rs = ps.executeQuery();
                while (rs.next()) {
                    arr.txt_AÑO.setText(rs.getString("AÑO"));
                    arr.txt_Mes.setText(rs.getString("MES"));
                    arr.txt_idArribo.setText("" + rs.getInt("id"));
                    arr.txt_idFac.setText("" + rs.getInt("id_Factura"));
                    arr.txt_fecha.setText(rs.getString("Fecha"));
                    arr.txt_Folio.setText(rs.getString("Folio"));
                    arr.cbx_Especie.setSelectedItem(rs.getString("Especie"));
                    arr.txt_ProdEscama.setText(rs.getString("Produccion_Escama"));
                    arr.txt_ProdPulpo.setText(rs.getString("Produccion_Pulpo"));
                    arr.txt_ProdCaracol.setText(rs.getString("Produccion_Caracol"));
                    arr.txt_Factura.setText(rs.getString("Factura"));
                    arr.txt_Facturado.setText(rs.getString("Facturado"));
                    arr.txt_montoFact.setText("" + rs.getInt("Monto"));
                    arr.txt_estado.setText(rs.getString("Estado"));
                }
                Botones(true, true, true);
                arr.txt_ProdCaracol.setEnabled(true);
                arr.txt_ProdPulpo.setEnabled(true);
                arr.txt_ProdEscama.setEnabled(true);
                String mes = arr.txt_Mes.getText();
                cargarTabla(mes);
                if (arr.txt_estado.getText().equals("Cancelado")) {
                    arr.btn_CancelarFactura.setText("Reingresar Factura");

                } else if (arr.txt_estado.getText().equals("Activo")) {
                    arr.btn_CancelarFactura.setText("Cancelar Factura");
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
