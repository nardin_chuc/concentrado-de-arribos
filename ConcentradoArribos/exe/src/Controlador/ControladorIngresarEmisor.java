/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.IngresarEmisorFactura;
import Vista.IngresarPescador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorIngresarEmisor implements ActionListener, MouseListener {

    IngresarEmisorFactura pes;

    int id, Num_Embarcacion;
    String Nombre, Apellido, Contraseña, Nombre_Embarcacion, RNPA, PermisoEscama, PermisoPulpo, PermisoCaracol,
            FechaInicioPermiso, FechaFinPermiso, FechaFielInicio, FechaFielFin, Estado;
    String rutaArribos;

    //***** CONSTRUCTOR PARA ACCIONAR LOS EVENTOS DE LA VISTA INGRESAR PERMISIONARIO
    public ControladorIngresarEmisor(IngresarEmisorFactura pes) {
        this.pes = pes;
        this.pes.btn_Cancelar.addActionListener(this);
        this.pes.btn_Desactivar.addActionListener(this);
        this.pes.btn_Ingresar.addActionListener(this);
        this.pes.btn_Modificar.addActionListener(this);
        this.pes.btn_Nuevo.addActionListener(this);
        this.pes.tb_Permisionario.addMouseListener(this);
        this.pes.tb_Permisionario2.addMouseListener(this);
        this.pes.btn_Eliminar.addActionListener(this);
        this.pes.btn_Eliminar.addMouseListener(this);
        Botones(false, false, true, true);
        cargarTabla();
        cargarTablaEmisor();
    }

    //*******METODO QUE RECIBE LOS EVENTOS EFECTUADOS
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == pes.btn_Ingresar) { //////******* BTN INGRESAR
            if (CamposVacios() == true) {
            } else {
                if (VericarTabla("El permisionario se encuentra en la Base de Datos") == true) {
                    Ingresar();
                    Botones(true, true, false, true);
                } else {
                }
            }
        } else if (e.getSource() == pes.btn_Modificar) {//////////******* BTN MODIFICAR
            if (CamposVacios() == true) {
                Limpiar();
            } else {
                Nombre = pes.txt_Nombre.getText();
                Apellido = pes.txt_Apellido.getText();
                String NombreTabla = Nombre.replace(" ", "") + "_" + Apellido.replace(" ", "");
                String nombreAux = pes.txt_NombreAUX.getText();
                if (VericarTabla("Se realizo correctamente") == true) {
                    Modificar();
                    CrearTablaSQL(Nombre, Apellido);
                    EliminarTabla(NombreTabla);
                    Botones(true, true, false, true);
                } else {
                    Modificar();
                    crearCarpeta(NombreTabla);
                }

            }
        } else if (e.getSource() == pes.btn_Desactivar) {/////////******** BTN ELIMINAR
            if (CamposVacios() == true) {
            } else {
                Eliminar();
                cargarTablaEmisor();
                Botones(true, true, false, true);
            }
        } else if (e.getSource() == pes.btn_Cancelar) { ///////////////////// ******* BTN CANCELAR
            int res = JOptionPane.showConfirmDialog(null, "¿Desea Cancelar la transaccion?");
            if (res == 0) {
                Limpiar();
                Botones(true, true, false, true);
            } else {

            }

        } else if (e.getSource() == pes.btn_Nuevo) {
            Limpiar();
            ActualizarId();
            Botones(true, false, true, true);
        } else if (e.getSource() == pes.btn_Eliminar) {
            if (CamposVacios()) {

            } else {
                EliminarRegistro();
                Limpiar();
                cargarTablaEmisor();
            }

        }

    }

    //********* METODO PARA INGRESAR NUEVOS PERMISIONARIOS A LA BASE DE DATOS
    public void Ingresar() {
        id = Integer.parseInt(pes.txt_id.getText());
        Nombre = pes.txt_Nombre.getText();
        Apellido = pes.txt_Apellido.getText();
        Contraseña = pes.txt_Contraseña.getText();
        RNPA = pes.txt_RNPA.getText();
        String rfc = pes.txt_RFC.getText();
        FechaInicioPermiso = pes.txt_VigenciaPermisoInicio.getText();
        FechaFinPermiso = pes.txt_VigenciaFinPermiso.getText();
        FechaFielInicio = pes.txt_FielInicio.getText();
        FechaFielFin = pes.txt_FielFin.getText();
        String cliente = pes.cbx_client.getSelectedItem().toString();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("INSERT INTO ListaFacturas  (id,Nombre, Apellido, Contraseña, "
                    + "RNPA, RFC, Fecha_Inicio_Permiso, Fecha_fin_Permiso, Fecha_Inicio_Fiel, Fecha_fin_Fiel, Clientes) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            ps.setInt(1, id);
            ps.setString(2, Nombre);
            ps.setString(3, Apellido);
            ps.setString(4, Contraseña);
            ps.setString(5, RNPA);
            ps.setString(6, rfc);
            ps.setString(7, FechaInicioPermiso);
            ps.setString(8, FechaFinPermiso);
            ps.setString(9, FechaFielInicio);
            ps.setString(10, FechaFielFin);
            ps.setString(11, cliente);
            ps.executeUpdate();
            cargarTabla();
            cargarTablaEmisor();
            CrearTablaSQL(Nombre, Apellido);
            JOptionPane.showMessageDialog(null, "Se Ingreso al Permisionario correctamente");
            pes.cbx_client.setSelectedIndex(0);
        } catch (Exception e) {
            System.out.println("error ingresar " + e.getMessage());
        }
    }

    //*********** MODIFICA LOS DATOS YA INGRESADOS DEL PERMISIONARIO
    public void Modificar() {
        id = Integer.parseInt(pes.txt_id.getText());
        Nombre = pes.txt_Nombre.getText();
        Apellido = pes.txt_Apellido.getText();
        Contraseña = pes.txt_Contraseña.getText();
        RNPA = pes.txt_RNPA.getText();
        String RFC = pes.txt_RFC.getText();
        FechaInicioPermiso = pes.txt_VigenciaPermisoInicio.getText();
        FechaFinPermiso = pes.txt_VigenciaFinPermiso.getText();
        FechaFielInicio = pes.txt_FielInicio.getText();
        FechaFielFin = pes.txt_FielFin.getText();
        String cliente = pes.cbx_client.getSelectedItem().toString();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("UPDATE ListaFacturas SET Nombre = ?, Apellido = ?, Contraseña = ?, "
                    + "RNPA = ?, RFC = ?, Fecha_Inicio_Permiso = ?,"
                    + "Fecha_fin_Permiso = ?, Fecha_Inicio_Fiel = ?, Fecha_fin_Fiel = ?, Clientes = ? WHERE id = ?");
            ps.setString(1, Nombre);
            ps.setString(2, Apellido);
            ps.setString(3, Contraseña);
            ps.setString(4, RNPA);
            ps.setString(5, RFC);
            ps.setString(6, FechaInicioPermiso);
            ps.setString(7, FechaFinPermiso);
            ps.setString(8, FechaFielInicio);
            ps.setString(9, FechaFielFin);
            ps.setString(10, cliente);
            ps.setInt(11, id);
            ps.executeUpdate();
            cargarTabla();
            cargarTablaEmisor();
            CrearTablaSQL(Nombre,Apellido);
            pes.cbx_client.setSelectedIndex(0);
            JOptionPane.showMessageDialog(null, "Se Modifico el Emisor correctamente");
        } catch (Exception e) {
            System.out.println("Error Modificar: " + e.getMessage());
        }
    }

    //COLOCA UNA ETIQUETA EN LA TABLA, QUE YA FUE ELIMINADO, PERO NO SE ELIMINA EN LA BASE DE DATOS
    public void Eliminar() {
        id = Integer.parseInt(pes.txt_id.getText());
        Estado = "Inactivo";
        if (pes.btn_Desactivar.getText().equals("Eliminar")) {
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE ListaFacturas SET Estado = ? WHERE id = ?");
                ps.setString(1, Estado);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTablaEmisor();
                JOptionPane.showMessageDialog(null, "Se Elimino al Permisionario correctamente");
                pes.btn_Desactivar.setText("Eliminar");
            } catch (Exception e) {
                System.out.println("error: " + e.getMessage());
            }
        } else if (pes.btn_Desactivar.getText().equals("Reingresar")) {
            String Estado2 = "Activo";
            try {
                Connection conectar = Conexion.establecerConnection();
                PreparedStatement ps = conectar.prepareStatement("UPDATE ListaFacturas SET Estado = ? WHERE id = ?");
                ps.setString(1, Estado2);
                ps.setInt(2, id);
                ps.executeUpdate();
                cargarTabla();
                JOptionPane.showMessageDialog(null, "Se Reingreso al Permisionario correctamente");
                pes.btn_Desactivar.setText("Eliminar");
            } catch (Exception e) {
                //System.out.println("error: "+e.getMessage());
            }
        }

    }

    public void EliminarRegistro() {
        id = Integer.parseInt(pes.txt_id.getText());
        String tabla = "Factura_" + pes.txt_NombreAUX.getText();
        try {
            Connection conectar = Conexion.establecerConnection();
            PreparedStatement ps = conectar.prepareStatement("DELETE ListaFacturas WHERE id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
            PreparedStatement ps2;
            ResultSet rs2;
            Connection con2 = Conexion.establecerConnection();
            ps2 = con2.prepareStatement("DROP TABLE " + tabla);
            rs2 = ps2.executeQuery();
            cargarTablaEmisor();
            JOptionPane.showMessageDialog(null, "Se Elimino de la Base de Datos al Emisor de Facturas correctamente");
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }

    }

    //********** CONDICION EN DONDE VERIFICAR SI LOS TXT NO ESTAN VACIOS, PARA PODER ACCEDER A UNA ACCION
    public boolean CamposVacios() {
        if (pes.txt_Nombre.getText().isEmpty() || pes.txt_Apellido.getText().isEmpty() || pes.txt_Contraseña.getText().isEmpty()
                || pes.txt_VigenciaPermisoInicio.getText().isEmpty() || pes.txt_id.getText().isEmpty() || pes.txt_RFC.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Digite los Campos vacios", "Campos Vacios", JOptionPane.ERROR_MESSAGE);
            return true;
        } else {
            return false;
        }
    }

    //**** VERIFICA EN LA BASE DE DATOS SI NO SE ENCUENTRA REPETIDA EL NOMBRE INGRESADO 
    public boolean VericarTabla(String textosino) {
        String NombreTabla = "Factura_" + pes.txt_Nombre.getText().replace(" ", "") + "_" + pes.txt_Apellido.getText().replace(" ", "");
        String nombreSql = "";
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("VerificarTablaFacturas ?");
            ps.setString(1, NombreTabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                nombreSql = (rs.getString("NombreTabla"));
            }
            if (nombreSql.isEmpty()) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, textosino);
                return false;
            }

        } catch (Exception e) {
            System.out.println(" Error: Verificar " + e.getMessage());
        }
        return false;
    }

    public void crearTbCliente() {
        String NombreTablaClientes = "Clientes_" + Nombre.replace(" ", "") + "_" + Apellido.replace(" ", "");
        try {
            Connection conectar = Conexion.establecerConnection();
            ResultSet rs;
            PreparedStatement ps = conectar.prepareStatement("EXECUTE CrearClientesTabla ?");
            ps.setString(1, NombreTablaClientes);
            rs = ps.executeQuery();
        } catch (Exception e) {
        }
    }

    //Crea Una tabla en SQL SERVER, POR EL MEDIO DE UN PROCEDIMIENTO ALMACENADO
    public void CrearTablaSQL(String Nombre, String Apellido) {
        String NombreCarpeta = Nombre.replace(" ", "") + "_" + Apellido.replace(" ", "");
        String NombreTabla = "Factura_" + Nombre.replace(" ", "") + "_" + Apellido.replace(" ", "");
        String opcionCliente = pes.cbx_client.getSelectedItem().toString();
        String nombreSql = "";

        crearCarpeta(NombreCarpeta);
        try {
            if (opcionCliente.equals("SI")) {
                crearTbCliente();
                Connection conectar = Conexion.establecerConnection();
                ResultSet rs;
                PreparedStatement ps = conectar.prepareStatement("EXECUTE CrearFacturasClientes ?");
                ps.setString(1, NombreTabla);
                rs = ps.executeQuery();
            } else {
                Connection conectar = Conexion.establecerConnection();
                ResultSet rs;
                PreparedStatement ps = conectar.prepareStatement("EXECUTE CrearTablaFacturas ?");
                ps.setString(1, NombreTabla);
                rs = ps.executeQuery();
            }

        } catch (Exception e) {
            System.out.println(" Error crear tabla clientes: " + e.getMessage());
        }
    }

    //********* CARGA LA TABLA QUE SE TIENE EN LA BASE DE DATOS
    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisionario.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 150, 150};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisionario.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id, Nombre, Apellido, Fecha_Inicio_Fiel, Fecha_fin_Fiel, Estado FROM Permisionario ORDER BY id ASC, Estado ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

    public void cargarTablaEmisor() {
        DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisionario2.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 150, 150, 150, 100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisionario2.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id, Nombre, Apellido, Contraseña, RFC, Fecha_fin_Permiso, Fecha_fin_Fiel, Estado FROM ListaFacturas ORDER BY id ASC, Estado ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

    //******* ACTUALIZA EL ID DE LOS PERMISIONARIOS, PARA PODER NGRESARLO O MODIFICARLO EN ALGUN FUTURO
    public void ActualizarId() {
        int idAux = 0;
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM ListaFacturas");
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            pes.txt_id.setText("" + idAux);
        } catch (Exception e) {
        }
    }

    //******* LIMPIA LOS CAMPOS EN EL CUAL SE INGRESO O MODIFICO
    public void Limpiar() {
        pes.txt_id.setText("");
        pes.txt_Nombre.setText("");
        pes.txt_Apellido.setText("");
        pes.txt_Contraseña.setText("");
        pes.txt_RNPA.setText("");
        pes.txt_VigenciaPermisoInicio.setText("");
        pes.txt_VigenciaFinPermiso.setText("");
        pes.txt_FielFin.setText("");
        pes.txt_FielInicio.setText("");
        pes.txt_estado.setText("");
    }

    //****** METODO QUE ELIMINA UNA TABLA EN SQL SERVE, EL CUAL SI SE ENCUENTRA REPETIDA, OMITE EL PASO
    //***** EN CASO CONTRARIO, LO ELIMINA
    public void EliminarTabla(String NombreTabla) {
        String NombreTablaAux = "Factura_" + pes.txt_NombreAUX.getText();
        try {
            if (NombreTabla.equals(NombreTablaAux)) {

            } else {
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("DROP TABLE " + NombreTablaAux);
                rs = ps.executeQuery();
            }
        } catch (Exception e) {

        }
    }

    //*******  CREA UNA CARPETA EN EL ESCRITORIO CON EL NOMBRE DEL PERMISIONARIO
    public void crearCarpeta(String nombreCarpeta) {
        String año = pes.lb_año.getText();
        String ruta = System.getProperty("user.home");
        rutaArribos = ruta + "\\OneDrive\\Escritorio\\"+ año + "_ConcentradoArribos\\" + nombreCarpeta + "\\Facturas";
        File f = new File(rutaArribos);
        if (f.mkdirs()) {
            System.out.println("Se creo Carpeta");

        } else {
            System.out.println("No se Creo la carpeta");
        }
    }

    public boolean VerificarEmisor(String nombre, String apellido) {
        try {
            String nombreAux = "";
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT Nombre FROM ListaFacturas WHERE Nombre = ? AND Apellido = ?");
            ps.setString(1, nombre);
            ps.setString(2, apellido);
            rs = ps.executeQuery();
            while (rs.next()) {
                nombreAux = rs.getString("Nombre");
            }
            if (nombreAux.isEmpty()) {
                return false;
            } else {
                JOptionPane.showMessageDialog(null, "Ya se encuentra en la Base de Datos emisor");
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    //******* EVENTO QUE AFECTA A LA TABLA QUE SE TIENE EN JAVA, PARA QUE SE PUEDA COLOCAR LOS DATOS DESEADOS EN LOS TXT
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == pes.tb_Permisionario) {
            try {
                int fila = pes.tb_Permisionario.getSelectedRow();
                int id = Integer.parseInt(pes.tb_Permisionario.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Nombre,Apellido, Contraseña, "
                        + "RNPA, Fecha_Inicio_PermisoEscama, Fecha_fin_PermisoEscama, Fecha_Inicio_Fiel, Fecha_fin_Fiel, Estado "
                        + "FROM Permisionario WHERE id = ?");
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pes.txt_id.setText("" + rs.getInt("id"));
                    pes.txt_Nombre.setText(rs.getString("Nombre"));
                    pes.txt_Apellido.setText(rs.getString("Apellido"));
                    pes.txt_Contraseña.setText(rs.getString("Contraseña"));
                    pes.txt_RNPA.setText(rs.getString("RNPA"));
                    pes.txt_VigenciaPermisoInicio.setText(rs.getString("Fecha_Inicio_PermisoEscama"));
                    pes.txt_VigenciaFinPermiso.setText(rs.getString("Fecha_fin_PermisoEscama"));
                    pes.txt_FielFin.setText(rs.getString("Fecha_fin_Fiel"));
                    pes.txt_FielInicio.setText(rs.getString("Fecha_Inicio_Fiel"));
                    pes.txt_estado.setText(rs.getString("Estado"));
                }
                String Nombre2 = pes.txt_Nombre.getText();
                String Apellido2 = pes.txt_Apellido.getText();
                pes.txt_NombreAUX.setText(Nombre2.replace(" ", "") + "_" + Apellido2.replace(" ", ""));
                if (VerificarEmisor(Nombre2, Apellido2)) {
                    Botones(true, true, false, true);
                } else {
                    Botones(true, true, true, true);
                }

                if (pes.txt_estado.getText().equals("Inactivo")) {
                    pes.btn_Desactivar.setText("Reingresar");
                } else if (pes.txt_estado.getText().equals("Activo")) {
                    pes.btn_Desactivar.setText("Eliminar");
                }
            } catch (Exception er) {
                System.err.println("Error en tabla: " + er.toString());
            }
        } else if (e.getSource() == pes.tb_Permisionario2) {
            try {
                int fila = pes.tb_Permisionario2.getSelectedRow();
                int id2 = Integer.parseInt(pes.tb_Permisionario2.getValueAt(fila, 0).toString());
                PreparedStatement ps;
                ResultSet rs;
                Connection con = Conexion.establecerConnection();
                ps = con.prepareStatement("SELECT id, Nombre,Apellido, Contraseña, "
                        + "RNPA, RFC, Fecha_Inicio_Permiso, Fecha_fin_Permiso, Fecha_Inicio_Fiel, Fecha_fin_Fiel, Estado, Clientes "
                        + "FROM ListaFacturas WHERE id = ?");
                ps.setInt(1, id2);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pes.txt_id.setText("" + rs.getInt("id"));
                    pes.txt_Nombre.setText(rs.getString("Nombre"));
                    pes.txt_Apellido.setText(rs.getString("Apellido"));
                    pes.txt_Contraseña.setText(rs.getString("Contraseña"));
                    pes.txt_RNPA.setText(rs.getString("RNPA"));
                    pes.txt_RFC.setText(rs.getString("RFC"));
                    pes.txt_VigenciaPermisoInicio.setText(rs.getString("Fecha_Inicio_Permiso"));
                    pes.txt_VigenciaFinPermiso.setText(rs.getString("Fecha_fin_Permiso"));
                    pes.txt_FielFin.setText(rs.getString("Fecha_fin_Fiel"));
                    pes.txt_FielInicio.setText(rs.getString("Fecha_Inicio_Fiel"));
                    pes.txt_estado.setText(rs.getString("Estado"));
                    pes.cbx_client.setSelectedItem(rs.getString("Clientes"));
                }
                Botones(true, true, false, true);
                String Nombre2 = pes.txt_Nombre.getText();
                String Apellido2 = pes.txt_Apellido.getText();
                pes.txt_NombreAUX.setText(Nombre2.replace(" ", "") + "_" + Apellido2.replace(" ", ""));
                if (pes.txt_estado.getText().equals("Inactivo")) {
                    pes.btn_Desactivar.setText("Reingresar");
                } else if (pes.txt_estado.getText().equals("Activo")) {
                    pes.btn_Desactivar.setText("Eliminar");
                }
            } catch (Exception er) {
                System.err.println("Error en tabla factura: " + er.toString());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == pes.btn_Eliminar) {
            pes.lb_avisoElimina.setVisible(true);
            pes.lb_avisoElimina.setText("Se eliminara de la Base de Datos");

        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == pes.btn_Eliminar) {
            pes.lb_avisoElimina.setVisible(false);

        }
    }

    public void Botones(boolean ver, boolean ver1, boolean ver2, boolean ver3) {
        pes.btn_Cancelar.setEnabled(ver);
        pes.btn_Desactivar.setEnabled(ver);
        pes.btn_Eliminar.setEnabled(ver);
        pes.btn_Modificar.setEnabled(ver1);
        pes.btn_Ingresar.setEnabled(ver2);
        pes.btn_Nuevo.setEnabled(ver3);

    }

}
