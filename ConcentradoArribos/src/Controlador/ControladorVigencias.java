
package Controlador;

import Conexion.Conexion;
import Modelos.TablaPermisos;
import Modelos.TablaVigencias;
import Vista.Menu;
import Vista.Vigencias;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class ControladorVigencias implements KeyListener{
    
    Vigencias pes;
    String año;
    public ControladorVigencias(Vigencias pes) {
        this.pes = pes;
        año = pes.txt_AÑO.getText();
        this.pes.txt_AÑO.addKeyListener(this);
        cargarTablaPermisos();
        cargarTablaFieles();
    }
    
    Menu mn;
    public ControladorVigencias(Menu mn) {
        this.mn =  mn;
       
    }
    
    public void cargarTablaPermisos() {
        String año = pes.txt_AÑO.getText();
        TablaPermisos tableColor = new TablaPermisos(año);
        pes.tb_Permisos.setDefaultRenderer(pes.tb_Permisos.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Permisos.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150, 150,150,150,150,150,100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Permisos.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id, Nombre, Apellido,Fecha_Inicio_PermisoEscama,Fecha_fin_PermisoEscama,Fecha_Inicio_PermisoPulpo,Fecha_fin_PermisoPulpo,"
                    + "Fecha_Inicio_PermisoCaracol, Fecha_fin_PermisoCaracol, Estado "
                    + "FROM Permisionario WHERE Fecha_fin_PermisoEscama LIKE '%"+año+"' OR "
                    + "Fecha_fin_PermisoCaracol LIKE '%"+año+"' OR Fecha_fin_PermisoPulpo LIKE '%"+año+"' ORDER BY id ASC";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }
    
    
    
    public void cargarTablaFieles() {
        TablaVigencias tableColor = new TablaVigencias();
        pes.tb_Fieles.setDefaultRenderer(pes.tb_Fieles.getColumnClass(0), tableColor);
        DefaultTableModel modeloTabla = (DefaultTableModel) pes.tb_Fieles.getModel();
        modeloTabla.setRowCount(0);
        PreparedStatement ps;
        ResultSet rs;
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {50, 150, 150, 150,100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            pes.tb_Fieles.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            Connection con = Conexion.establecerConnection();
            String sql = "SELECT id, Nombre,Apellido, Fecha_fin_Fiel, Estado "
                    + "FROM Permisionario WHERE Fecha_fin_Fiel LIKE '%"+año+"' ORDER BY id"
                    + " ASC" ;
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            rsmd = rs.getMetaData();
            columnas = rsmd.getColumnCount();

            while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
            }
        } catch (Exception e) {
            System.err.println("Error en tabla: " + e.toString());
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
   }

    @Override
    public void keyPressed(KeyEvent e) {
   }

    @Override
    public void keyReleased(KeyEvent e) {
     if(e.getSource() == pes.txt_AÑO){
         año = pes.txt_AÑO.getText();
         cargarTablaFieles();
         cargarTablaPermisos();
     }
    }
    
    
    
    
}
