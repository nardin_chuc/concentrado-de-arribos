/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Entidades.EEspecies;
import Vista.frmRegistroArriboPDF;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 *
 * @author CCNAR
 */
public class ControladorfrmRegistroArriboPDF implements ActionListener {

    frmRegistroArriboPDF frmArrPdf;
    String pathArchivo;
    String[] lineas;
    List<EEspecies> especies = new ArrayList<EEspecies>();

    public ControladorfrmRegistroArriboPDF(frmRegistroArriboPDF frmArrPdf) {
        this.frmArrPdf = frmArrPdf;
        ListaEspecies();
        this.frmArrPdf.btn_buscarArchivo.setFocusable(true);
        this.frmArrPdf.btn_registrarArribo.addActionListener(this);
        this.frmArrPdf.btn_buscarArchivo.addActionListener(this);
        this.frmArrPdf.btn_leerArchivo.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == frmArrPdf.btn_registrarArribo) {
            Ingresar();
        } else if (e.getSource() == frmArrPdf.btn_buscarArchivo) {
            BuscarArchivo();
        } else if (e.getSource() == frmArrPdf.btn_leerArchivo) {
            LeerArchivo();
            ObtenerDatosArribo();
            ObtenerProductos();
        }
    }

    private void BuscarArchivo() {
        JFileChooser fileChooser = new JFileChooser();
        int seleccion = fileChooser.showOpenDialog(null);

        if (seleccion == JFileChooser.APPROVE_OPTION) {
            var archivo = fileChooser.getSelectedFile();
            frmArrPdf.txt_path.setText(archivo.getAbsolutePath());
            pathArchivo = archivo.getAbsolutePath();
        }
    }

    private void LeerArchivo() {
        try (PDDocument document = PDDocument.load(new File(pathArchivo))) {
            PDFTextStripper pdfStripper = new PDFTextStripper();
            String texto = pdfStripper.getText(document);
            lineas = texto.split("\n");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error en la Lectura del Archivo.", "Error", JOptionPane.ERROR);
        }
    }

    private void ObtenerProductos() {
        DefaultTableModel modeloTabla = (DefaultTableModel) frmArrPdf.tb_Productos.getModel();
        modeloTabla.setRowCount(0);
        try {
            int i = 0;
            for (String linea : lineas) {
                i++;
                if (i >= 27) {
                    String[] partes = linea.split(" ");

                    if (!especies.stream().anyMatch(e -> e.Especie.contains(partes[0].replace("-", " ").trim()))) {
                        break;
                    }

                    Object[] fila = {partes[0].replace("-", " ").trim().toString(), partes[partes.length - 3].trim(), partes[partes.length - 1].trim(), 0};
                    modeloTabla.addRow(fila);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error en la Lectura de Productos del Archivo.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void ObtenerDatosArribo() {
        var encontrado = false;
        try {
            for (String linea : lineas) {
                if (linea.contains("Cadena Original")) {
                    encontrado = true;
                    continue;
                }
                if (encontrado) {
                    String[] partes = linea.split(" \\| ");
                    frmArrPdf.txt_Folio.setText(partes[3].trim());
                    var fecha = partes[4].trim().split("/");
                    frmArrPdf.txt_fecha.setText(fecha[0]+"-"+fecha[1]+"-"+fecha[2].substring(2));
                    Mes(fecha[1]);
                    frmArrPdf.txt_AÑO.setText(fecha[2]);
                    break;
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error en la Lectura de Productos del Archivo.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void ListaEspecies() {
        try {
            especies = new ArrayList<EEspecies>();
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT * FROM Especies WHERE Estado = 'Activo'");
            rs = ps.executeQuery();
            while (rs.next()) {
                especies.add(new EEspecies(Integer.parseInt(rs.getString("id")), rs.getString("Especie"), rs.getInt("Precio")));
            }
            rs.close();

        } catch (Exception er) {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error en la Cosnulta de Especies", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void Mes(String mesNumber) {
        if (mesNumber.equals("01")) {
            frmArrPdf.txt_Mes.setText("ENE");
        } else if (mesNumber.equals("02")) {
            frmArrPdf.txt_Mes.setText("FEB");
        } else if (mesNumber.equals("03")) {
            frmArrPdf.txt_Mes.setText("MAR");
        } else if (mesNumber.equals("04")) {
            frmArrPdf.txt_Mes.setText("ABR");
        } else if (mesNumber.equals("05")) {
            frmArrPdf.txt_Mes.setText("MAY");
        } else if (mesNumber.equals("06")) {
            frmArrPdf.txt_Mes.setText("JUNE");
        } else if (mesNumber.equals("07")) {
            frmArrPdf.txt_Mes.setText("JULY");
        } else if (mesNumber.equals("08")) {
            frmArrPdf.txt_Mes.setText("AGOST");
        } else if (mesNumber.equals("09")) {
            frmArrPdf.txt_Mes.setText("SEP");
        } else if (mesNumber.equals("10")) {
            frmArrPdf.txt_Mes.setText("OCT");
        } else if (mesNumber.equals("11")) {
            frmArrPdf.txt_Mes.setText("NOV");
        } else if (mesNumber.equals("12")) {
            frmArrPdf.txt_Mes.setText("DIC");
        } else {
            frmArrPdf.txt_Mes.setText("");
        }
    }

    public void Ingresar() {
        String año = frmArrPdf.txt_AÑO.getText();
        String mes = frmArrPdf.txt_Mes.getText();
        String fecha = frmArrPdf.txt_fecha.getText();
        int id = Integer.parseInt(frmArrPdf.txt_idArribo.getText());
        String folio = frmArrPdf.txt_Folio.getText();
        String estado = "Activo";
        String nombreTabla = frmArrPdf.txt_NombreArribo.getText().replace(" ", "") + "_" + frmArrPdf.txt_Apellido.getText().replace(" ", "");

        int rowCount = frmArrPdf.tb_Productos.getRowCount();

        Connection conectar = null;
        PreparedStatement ps = null;
        try {
            conectar = Conexion.establecerConnection();
            for (int row = 0; row < rowCount; row++) {
                String Especie = (String) frmArrPdf.tb_Productos.getValueAt(row, 0);
                float produccion_escama = (!Especie.toLowerCase().equals("pulpo") && !Especie.toLowerCase().equals("caracol")) ? Float.parseFloat(frmArrPdf.tb_Productos.getValueAt(row, 1).toString()) : 0;
                float produccion_Pulpo = Especie.toLowerCase().equals("pulpo") ? Float.parseFloat(frmArrPdf.tb_Productos.getValueAt(row, 1).toString()) : 0;
                float produccion_Caracol = Especie.toLowerCase().equals("caracol") ? Float.parseFloat(frmArrPdf.tb_Productos.getValueAt(row, 1).toString()) : 0;
                float precio = Float.parseFloat(frmArrPdf.tb_Productos.getValueAt(row, 2).toString());
                int diaPesca = Integer.parseInt(frmArrPdf.tb_Productos.getValueAt(row, 3).toString());
                float totalEscama = produccion_escama * precio;
                float totalPulpo = produccion_Pulpo * precio;
                float totalCaracol = produccion_Caracol * precio;

                ps = conectar.prepareStatement("INSERT INTO " + nombreTabla
                        + "(AÑO, MES, Fecha,id,Folio, Especie, Produccion_Escama, Produccion_Pulpo, Produccion_Caracol, "
                        + "precio, DiasDe_Pesca, TotalEscama, TotalPulpo, TotalCaracol, Estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                ps.setString(1, año);
                ps.setString(2, mes);
                ps.setString(3, fecha);
                ps.setInt(4, id);
                ps.setString(5, folio);
                ps.setString(6, Especie);
                ps.setFloat(7, produccion_escama);
                ps.setFloat(8, produccion_Pulpo);
                ps.setFloat(9, produccion_Caracol);
                ps.setFloat(10, precio);
                ps.setInt(11, diaPesca);
                ps.setFloat(12, totalEscama);
                ps.setFloat(13, totalPulpo);
                ps.setFloat(14, totalCaracol);
                ps.setString(15, estado);
                ps.executeUpdate();
            }
            Limpiar();
            limpiarTable();
            ActualizarId();
            JOptionPane.showMessageDialog(null, "Se Ingreso el arribo correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error en el Guardado del Arribo: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            // Asegura que el PreparedStatement se cierre incluso si hay un error
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conectar != null) {
                    conectar.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void SetValuesPermisionario(String nombre, String apellido) {
        frmArrPdf.txt_NombreArribo.setText(nombre);
        frmArrPdf.txt_Apellido.setText(apellido);
        ActualizarId();
    }

    public void ActualizarId() {
        int idAux = 0;
        String nombreTabla = frmArrPdf.txt_NombreArribo.getText().replace(" ", "") + "_" + frmArrPdf.txt_Apellido.getText().replace(" ", "");
        try {
            PreparedStatement ps;
            ResultSet rs;
            Connection con = Conexion.establecerConnection();
            ps = con.prepareStatement("SELECT MAX(id) AS id_Maximo FROM " + nombreTabla);
            rs = ps.executeQuery();
            while (rs.next()) {
                idAux = (rs.getInt("id_Maximo") + 1);
            }
            rs.close();
            frmArrPdf.txt_idArribo.setText("" + idAux);
        } catch (Exception e) {
        }
    }
    
    public void limpiarTable() {
        DefaultTableModel modeloTabla = (DefaultTableModel) frmArrPdf.tb_Productos.getModel();
        modeloTabla.setRowCount(0);
        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
            modeloTabla.removeRow(i);
            i = i - 1;
        }
    }
    
        public void Limpiar() {
        frmArrPdf.txt_Folio.setText("");
        frmArrPdf.txt_path.setText("");
        pathArchivo = "";

    }
}
