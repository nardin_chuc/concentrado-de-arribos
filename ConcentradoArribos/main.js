// Función para mostrar el spinner
function mostrarSpinner() {
    $('#spinner').show();
}

// Función para ocultar el spinner
function ocultarSpinner() {
    $('#spinner').hide();
}